<?php 
	// Created by Gian Pompilio, HRM Web Services (September 24-09)
	// Email pompilg@halifax.ca for information
 	
	// Requirements
	require_once( "init.php" );	// Initialize
	CheckLoginStatus(1);		// 1 = admins only

	// Process submitted information
	if( isset( $_POST['fState'] ) ) {

		UserInsertUpdate( $_POST );
	
	} else {

		// Intialized form (pre-populate values if appropriate)
		$FormTitle = 'Add User';
		$FormState = 'insert';	// default to insert (for new users)
		
		if( isset( $_GET['id'] ) ) {
		
			$FormState = 'update';
			$UserValues = getUserDetails( $_GET['id'] );
			
			if( $UserValues ) {
			
				$FormState = 'update'; 	// update mode
				$FormTitle = 'Edit User';
				
			} else {
			
				echo 'Error gathering user data';
				
			}
				
		}
		
	}

	// Add common header
	require_once( $Server.$SysRootInc."Header.php" );

?>
    
    <form id="UserForm" name="UserForm" method="post" action="User.php" onsubmit="<?php  if( !$UserValues ) { ?>RandomPasswordGenerator(document.UserForm.fPassword); <?php  } ?>return UserFormValidation(this);">
    
        <table class="User">
            
            <tr><td colspan="2" class="Label MenuDivider"><?php  echo $FormTitle; ?></td></tr>
            
			<?php  
			
				if( $_GET['msg'] == 1 ) {
				
					echo '<tr><td colspan="2"><p class="ErrorMsg"><strong>ERROR:</strong> This user is a super administrator, their information cannot be modified.</p></td></tr>';
					
				} elseif ( $_GET['msg'] == 2 ) {

					echo '<tr><td colspan="2"><p class="ErrorMsg"><strong>ERROR:</strong> A user with this email address already exists (details below). Duplicate email addresses are not permitted. </p></td></tr>';

				}

			?>

            <tr>
                <td>Name:</td>
                <td>
                	<input id="fFirstName" name="fFirstName" type="text" class="FirstName" value="<?php  echo $UserValues[1]; ?>" />
                	<input id="fLastName" name="fLastName" type="text" class="LastName" value="<?php  echo $UserValues[2]; ?>" />
                </td>
            </tr>
            
            <tr>
                <td>E-Mail:</td>
                <td><input id="fEmail" name="fEmail" type="text" class="Email" value="<?php  echo $UserValues[3]; ?>" /></td>
            </tr>
            
            <tr>
                <td>User type:</td>
                <td>
                	<select id="fUserType" name="fUserType" onChange="showhidefield(document.UserForm.fUserType)" style="width:65px;">
                        <option value="3" <?php  if( $UserValues[5] == 3 ) echo "Selected"; ?>>User</option>
                        <option value="2" <?php  if( $UserValues[5] == 2 ) echo "Selected"; ?>>OpsO</option>
                        <option value="1" <?php  if( $UserValues[5] == 1 ) echo "Selected"; ?>>Admin</option>
                    </select>
                </td>
            </tr>


            <tr>
                <td>Active:</td>
                <td><input id="fActive" name="fActive" type="checkbox" class="CheckBox" value="1" <?php  if( $UserValues[6] != '0' ) echo "Checked"; ?> /><label for="fActive">Yes</label></td>
            </tr>
                           
            <tr>
                <td style="vertical-align:top;">Category:</td>
                <td>
                
                    <span id="SelectCategories">
                        <p>Check all that apply:</p>
                        <?php  listCategoryCheckboxes( $UserValues[0] ); ?>
                    </span> 
                    
                    <span id="AdminUser" style="display:none;">
                        <p>Can access all categories and create, modify, or delete other users.</p>
                    </span>
                    
                    <span id="OpsOUser" style="display:none;">
                        <p>Can access all categories.</p>
                    </span>
                
                </td>
            </tr>
                   
            <tr>
                <td></td>
                <td><input class="Button" type="submit" value="Submit" name="submitform" /></td>
            </tr>

            <tr> 
                <td></td>
                <td><p>A welcome email will be sent to the user containing login instructions and an automatically generated password.<p></td>
            </tr>
    
        </table>
        
        <input name="fPassword" type="hidden" id="fPassword" value="<?php  echo $UserValues[4]; ?>" />
        <input type="hidden" name="fID" id="fID" value="<?php  echo $UserValues[0]; ?>" />
        <input type="hidden" name="fState" id="fState" value="<?php  echo $FormState; ?>" />
		
        
    </form>
    
    <script type="text/javascript" language="javascript">
		window.onload=function(){
			showhidefield(document.UserForm.fUserType);
		}
	</script>
    
<?php  require_once( $Server.$SysRootInc."footer.php" ); ?>