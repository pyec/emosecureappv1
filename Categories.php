<?php
  /*---------------------------------------------------------------
   * 
   * 	MODULE:		categories.php
   * 	AUTHOR:		Gian
   * 	Created:	Unknown
   * 
   * --------------------------------------------------------------
   * 
   * 	
   * 	MODIFICATION HISTORY
   * 	20170519	PRSC	Cleanup and PHP tabs fixed.
   *    * 
   *---------------------------------------------------------------
   */
	// Created by Gian Pompilio, HRM Web Services (September 24-09)
	// Contact pompilg@halifax.ca for information
 	
	// Load functions
	
	require_once( "init.php" );
	CheckLoginStatus(1);	// 1 = admins only


	if( isset( $_REQUEST['fState'] ) ) {

		global $conn;
		dbConnect();

		switch( $_REQUEST['fState'] ) {

			case "Update":

				// Assign new category access rights
				foreach( $_POST as $key => $value ) {
					if($key != "fState" && $key != "fState") {
							$sql = "UPDATE Categories SET Name = '".$value."' WHERE CID = '".substr( $key, 1 )."'";
							$rs = sqlsrv_query( $conn, $sql ) or die( 'Error updating category information.' );
					}
				}

				break;


			case "Add":

				$sql = "INSERT INTO Categories (Name) VALUES('".$_POST['fCategoryName']."')";
				$rs = sqlsrv_query( $conn, $sql ) or die( 'Error inserting new category.' );

				$sql = "SELECT * FROM Categories WHERE Name = '".$_POST['fCategoryName']."'";
				$rs = sqlsrv_query( $conn, $sql ) or die( 'Error inserting new category.' );

				$row = sqlsrv_fetch_array( $rs );

//				if( $row = sqlsrv_fetch_array( $rs ) ) {			
//					echo $row["CID"].": ".$row["Name"];
//				}
				
				if( isset( $row["CID"] ) ) {

					$id = $row["CID"];
				
					mkdir( 'D:/Websites/HRM/EMOSecureApp/Categories/'.$id );
					$file = 'D:/Websites/HRM/EMOSecureApp/Categories/Template.txt';
					$newfile = "D:/Websites/HRM/EMOSecureApp/Categories/".$id."/index.php";
	
					if (!copy($file, $newfile)) {
						echo "failed to copy $file...\n";
					}
	
	
					$handle = fopen( $newfile, 'r' ) or die("Can't open file for reading.");
					$contents = fread( $handle, filesize( $newfile ) ) or die("Can't read file.");
					fclose( $handle );

					echo "<pre>";
					print_r($contents);
					echo "</pre>";
					
					$new_contents = str_replace( '%CatNum%', $id, $contents );

					// die($new_contents);

					echo "<pre>";
					print_r($new_contents);
					echo "</pre>";
					
					 
	
					$handle = fopen( $newfile, 'w' ) or die("Can't open file for writing.");
					fwrite( $handle, $new_contents );
					fclose( $handle ); 

				}
	
				break;


			case "Delete":
				
				//echo 'Delete '.$_REQUEST['cid'];

				//Remove from category table
				$sql = "DELETE FROM Categories WHERE CID = '".$_REQUEST['cid']."'";
				$rs = sqlsrv_query( $conn, $sql ) or die( 'Error inserting new category.' );

				//Remove from user access table
				$sql = "DELETE FROM CategoryAccess WHERE CID = '".$_REQUEST['cid']."'";
				$rs = sqlsrv_query( $conn, $sql ) or die( 'Error inserting new category.' );

				//Delete Category file from server
				//unlink('D:/Websites/HRM/EMOSecureApp/Categories/'.$_REQUEST["cid"].'.html');
				//echo rmdir( 'D:/Websites/HRM/EMOSecureApp/Categories/'.$_REQUEST['cid'] );
				//remove_dir( 'D:/Websites/HRM/EMOSecureApp/Categories/'.$_REQUEST['cid'] );
				RemoveDirectory( 'D:/Websites/HRM/EMOSecureApp/Categories/'.$_REQUEST['cid'] );

				break;


			case "Archive":
				
				$msg = '<a href="/EMOSecureApp/Categories/Archives/'.$_REQUEST["title"].'">'.$_REQUEST['title'].'</a> has been archived.';

				if( $row = sqlsrv_fetch_array( $rs ) ) {			
					echo $row["CID"].": ".$row["Name"];
				}
				
				//smartCopy( 'D:/Websites/HRM/EMOSecureApp/Categories/'.$_REQUEST["cid"], 'D:/Websites/HRM/EMOSecureApp/Categories/Archives/'.$_REQUEST["cid"] );
				// if ( !rename( "D:/Websites/HRM/EMOSecureApp/Categories/".$_REQUEST["cid"], "D:/Websites/HRM/EMOSecureApp/Categories/Archives/".$_REQUEST["title"] ) ) {
				if ( !smartCopy( 'D:/Websites/HRM/EMOSecureApp/Categories/'.$_REQUEST["cid"], 'D:/Websites/HRM/EMOSecureApp/Categories/Archives/'.$_REQUEST["title"] )) {
					echo "failed to move directory...";
				}
				
				$newfile = 'D:/Websites/HRM/EMOSecureApp/Categories/Archives/'.$_REQUEST["title"].'/index.php';
/*				
				$file = 'D:/Websites/HRM/EMOSecureApp/Categories/'.$_REQUEST["cid"].'.html';
				$newfile = 'D:/Websites/HRM/EMOSecureApp/Categories/Archives/'.$_REQUEST["cid"].'/'.$_REQUEST["title"].'.php';

				if (!copy($file, $newfile)) {
					echo "failed to copy $file...\n";
				}
				
*/


				$handle = fopen( $newfile, 'r' ) or die("Can't open file.");
				$contents = fread( $handle, filesize( $newfile ) ) or die("Can't read file.");
				$title = "<? echo pageTitle(".$_REQUEST['cid']."); ?>";
				$new_contents = str_replace( $title, str_replace("-"," ",$_REQUEST["title"]), $contents );
				fclose( $handle ); 

				$handle = fopen( $newfile, 'w+' );
				fwrite( $handle, $new_contents );
				fclose( $handle );

				$output = 'Categories.php?msg='.$msg;


				//Remove from category table
				$sql = "DELETE FROM Categories WHERE CID = '".$_REQUEST['cid']."'";
				$rs = sqlsrv_query( $conn, $sql ) or die( 'Error inserting new category.' );

				//Remove from user access table
				$sql = "DELETE FROM CategoryAccess WHERE CID = '".$_REQUEST['cid']."'";
				$rs = sqlsrv_query( $conn, $sql ) or die( 'Error inserting new category.' );

				break;		

		}

		sqlsrv_close( $conn );

		if( isset( $output ) ) {
			header( 'Location: Categories.php?msg='.$msg );
		}

	}



	// Begin Assembling Page
	
	require_once ( $Server.$SysRootInc."Header.php" );
			
	listCategoryAdmin( $_REQUEST['msg'] );
			   
	require_once ( $Server.$SysRootInc."footer.php" ); 

?>
