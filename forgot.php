 <?php 
  /*---------------------------------------------------------------
   * 
   * 	MODULE:		forgot.php
   * 	AUTHOR:		Gian
   * 	Created:	Unknown
   * 
   * 	This module handles forgotten password functionality
   * 
   * --------------------------------------------------------------
   * 
   * 	
   * 	MODIFICATION HISTORY
   * 	20170519	PRSC	Cleanup and PHP tabs fixed.
   *     
   *---------------------------------------------------------------
   */
	
	// Load functions
	require_once( "init.php" );
	session_destroy();

	// Process submitted information
	if( isset( $_POST['fUserEmail'] ) ) {

		global $conn;
		dbConnect();

		// echo $_POST['fUserEmail'];

		// if( $conn ) {
		//      echo "Connection established.<br />";
		// }else{
		//      echo "Connection could not be established.<br />";
		//      die( print_r( sqlsrv_errors(), true));
		// }

		// SQL
		$sql = "SELECT * FROM Users WHERE Email = '".$_POST['fUserEmail']."'";
		// echo $sql;

		$rs = sqlsrv_query( $conn, $sql );
		// echo $rs;

		$row = sqlsrv_fetch_array($rs, SQLSRV_FETCH_ASSOC);
		// echo "<pre>";
		// print_r($row);
		// echo "</pre>";
		

		if( !empty($row) ) {
			header("Location: https://apps.halifax.ca/EMODocExchange/sendmail.php?n=".$row['FirstName']."&u=".$row['Email']."&p=".$row['Password']."&te=".$row['Email']."&t=2");

		} else {
			header('Location: https://www.halifax.ca'.$Root.'forgot.php?x=6');
		}

		sqlsrv_close( $conn );

	}

	CheckVars( $_REQUEST['x'] );
	

 	require_once ( $Server.$SysRootInc."HeaderMetaEtc.php" ); 

?>

		<div id="Login" class="GStyle LessPadding">
	        
        	<div style="padding:25px !important;">     
             
                <h1><?php echo $Title; ?></h1>
                
                <form action="forgot.php" method="post" name="fEMOSecure" id="EMOSecureLogin" onSubmit="return ForgotValidation(this);">
                    <table style="padding-top:5px;">
                    	<?php echo $ErrorMsg; ?>
                        <tr>
                            <td colspan="2" class="Description">To retrieve your login credentials, enter your email address below.</td>
                        </tr>
                        <tr>
                            <td class="Label"><label for="fEmail">Email: </label></td>
                            <td><input id="fUserEmail" name="fUserEmail" type="text" maxlength="50" /></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="padding-top:5px;"><input id="fSubmit" name="fSubmit" class="Button" type="Submit" value="Send" /><a href="index.php">Return to login</a></td>
                        </tr>                       
                    </table>
                </form>
                
<?php	require_once ( $Server.$SysRootInc."footer.php" ); ?>