
<h3><? echo pageTitle(60); ?></h3>
<p style="margin-bottom: 0;">Click a link below to jump to that section and get the latest documents.</p>

<p style="margin-top: 0; margin-bottom: 0;">&nbsp;</p>
<table class="DocLinks">
  <tr>
    <td>
      <ul>
        <li><a href="#Guidance">Guidance</a></li>
        <li><a href="#Exercise Plan">Exercise Plan</a></li>
        <li><a href="#Questionnaire">Questionnaire</a></li>
      </ul>
    </td>
    <td>
      <ul>
	    <li><a href="#Maps">Maps</a></li>
	    <li><a href="#Observation">Observation</a></li>
	    <li><a href="#Appendices">Appendices</a></li>
      </ul>
    </td>
  </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
	<td><a name="Guidance" id="Updates"></a>Guidance</td>
  </tr>
  <tr>
    <td>
      <ul>      
        <li><a href="/EMOSecureApp/Categories/60/documents/CAO_GUIDANCE_HEREX_12_v03.pdf">CAO Guidance V03</a> - [pdf]</li>
      </ul>
    </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
	<td><a name="ExercisePlan" id="Reports"></a>Exercise Plan</td>
  </tr>
  <tr>
    <td>
      <ul>
        <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_00_final.pdf">Exercise Plan</a> - [pdf]</li>
        <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_00_Doc_Control_120624.pdf">Document Control </a>- [pdf]</li>
        <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EPG_ToR_v01.pdf">Terms of Reference </a>- [pdf]</li>
      </ul>
    </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
    <td><a name="Questionnaire" id="Logs2"></a>Questionnaire</td>
  </tr>
  <tr>
    <td><ul>
      <li><a href="/EMOSecureApp/Categories/60/documents/AAQ-EOCCommand.pdf">EOC Command</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/AAQ-EOCOpsSection.pdf">EOC Ops Section</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/AAQ-EOCPIO.pdf">EOC PIO</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/AAQ-HRP.pdf">HRP</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/AAQ-RCMP.pdf">RCMP </a>[pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/AAQ-HRFE.pdf">Fire</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/AAQ-Transit.pdf">Transit</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/AAQ-TPW.pdf">TPW</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/AAQ-HW.pdf">HW</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/AAQ-GIS.pdf">GIS</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/AAQ-CDHA.pdf">CDHA</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/AAQ-CF.pdf">CF</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/AAQ-PS.pdf">PS</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/AAQ-EXCON.pdf">EXCON</a> [pdf]</li>
    </ul></td>
  </tr>
</table>
<table class="DocumentListings">
  <tr class="Title">
	<td><a name="Maps" id="Logs"></a>Maps</td>
  </tr>
  <tr>
    <td>
      <ul>
        <li><a href="/EMOSecureApp/Categories/60/documents/HEREX12_letter.pdf">Harbour Area </a>(letter size) [pdf]</li>
        <li><a href="/EMOSecureApp/Categories/60/documents/HEREX12_ledger.pdf">Harbour Area</a>&nbsp;(ledger size) [pdf]</li>
      </ul>
    </td>
  </tr>
</table>
<table class="DocumentListings">
  <tr class="Title">
	<td><a name="Observation" id="Contacts"></a>Observation</td>
  </tr>
  <tr>
    <td><ul>
	    <li><a href="/EMOSecureApp/Categories/60/documents/Obsv-EOCCommand.pdf">EOC Command </a>[pdf]</li>
	    <li><a href="/EMOSecureApp/Categories/60/documents/Obsv-EOCOpsSection.pdf">EOC Ops Section </a>[pdf]</li>
	    <li><a href="/EMOSecureApp/Categories/60/documents/Obsv-HRP.pdf">HRP</a> [pdf]</li>
	    <li><a href="/EMOSecureApp/Categories/60/documents/Obsv-Transit.pdf">Transit</a> [pdf]</li>
	    <li><a href="/EMOSecureApp/Categories/60/documents/Obsv-TPW.pdf">TPW</a>&nbsp;[pdf]</li>
	    <li><a href="/EMOSecureApp/Categories/60/documents/Obsv-HW.pdf">HW</a> [pdf]</li>
	    <li><a href="/EMOSecureApp/Categories/60/documents/Obsv-CDHA.pdf">CDHA</a> [pdf]</li>
	    <li><a href="/EMOSecureApp/Categories/60/documents/Obsv-CF.pdf">CF</a> [pdf]</li>
	    <li><a href="/EMOSecureApp/Categories/60/documents/Obsv-EXCON.pdf">EXCON</a> [pdf]</li>
        </ul>
    </td>
  </tr>
</table>
<table class="DocumentListings">
  <tr class="Title">
    <td><a name="Appendices" id="Contacts2"></a>Appendices</td>
  </tr>
  <tr>
    <td><ul>
      <li><a href="/emosecureapp/Categories/60/documents/HEREX_12_EXPLAN_01_Participants_v02.pdf">Appendix 01 - Participants</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_02_Planners_v08.pdf">Appendix 02 - Planners</a>&nbsp;[pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_03_EXCON_v06.pdf">Appendix 03 - Exercise Control</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_04_HRM_EOC_v02.pdf">Appendix 04 - EOC Layout</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_05_EOC_Members_v12.pdf">Appendix 05 - EOC Members </a>[pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_06_HRM_EOC_Tel_v01.pdf">Appendix 06 - EOC Phone Numbers</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_07_EXCON_Tel_v02.pdf">Appendix 07 - ExCon&nbsp;Phone Numbers </a>[pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_10_Maps_v02.pdf">Appendix 10 - Maps</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_11_Wind_v01.pdf">Appendix 11 V01 - Wind </a>[pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_13_Media_Observers_Visitors.pdf">Appendix 13 - Media Observers</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_13_Media_v01.pdf">Appendix 13 - Media Operations</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_14_Media_Availability.pdf">Appendix 14 - Media Availability</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_14_Press_Conference_v01.pdf">Appendix 14 - Press Conference</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_15_RTQ_v01.pdf">Appendix 15 - RTQ</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_16_Media_Advisories_v01.pdf">Appendix 16 - Media Advisories </a>[pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_16_Release_1.pdf">Appendix 16 - Release 01</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_16_Release_2.pdf">Appendix 16 - Release 02</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_16_Release_3.pdf">Appendix 16 - Release 03</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_16_Release_4.pdf">Appendix 16 - Release 04</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_17_SOLE_v01.pdf">Appendix 17 - SOLE</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_18_RFA_v01.pdf">Appendix 18 - RFA</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_22_Significant_Terms_v02.pdf">Appendix 22 - Significant Terms </a>[pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_25_Writing_Conventions_v02.pdf">Appendix 25 - Writing Conventions </a>[pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_31_FSS_DENVILLE_v02.pdf">Appendix 31 - FSS Denville </a>[pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_35_Task_List_v13.pdf">Appendix 35 - Task List</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_37_Level_of_Effort_v04.pdf">Appendix 37 - Level of Effort</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_38_Seminar_v11.pdf">Appendix 38 - Seminar</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_39_Training_v13.pdf">Appendix 39 - Training</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_40_Post-Exercise_v01.pdf">Appendix 40 - Post Exercise </a>[pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_43_EOC_v07.pdf">Appendix 43 - EOC</a> [pdf]</li>
      <li><a href="/EMOSecureApp/Categories/60/documents/HEREX_12_EXPLAN_44_RC_Initial_Brief_v02.pdf">Appendix 44 - Initial Brief</a> [pdf]</li>
    </ul></td>
  </tr>
</table>
<p style="margin-top: 0; margin-bottom: 0">&nbsp;</p>
