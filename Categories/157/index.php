<h3><?= pageTitle(157) ?></h3>
<p>Click a link below to jump to that section and get the latest documents.</p>

<table class="DocLinks">
    <tr>
        <td width="101">
            <ul>
              <li><a href="#Updates">Updates</a></li>
                <li></li>
              <li><a href="#Reports">Reports</a></li>
       	  </ul>
        </td>
      <td width="96">
        <ul>
		  <li><a href="#Logs">Logs</a></li>
		  <li><a href="#Contacts">Contacts</a></li>
   	    </ul>
      </td>
  </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Updates" id="Updates"></a>Updates</td>
  </tr>
  <tr>
        <td>
        <ul>      
          <li>Updates- 20 KB [pdf]</li>
          <li><a href="https://www.halifax.ca/EMOSecureApp/Categories/157/documents/Winter_Storm_udpate2.pdf" target="_blank">Weather Update 2</a> - 25 KB [jpg]</li>
          <li><a href="" target="_blank">Update 3</a> - 10 KB [gif]</li>
        </ul>
        </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Reports" id="Reports"></a>Reports</td>
  </tr>
  <tr>
        <td>
        <ul>
          <li><a href="/EMOSecureApp/Categories/157/documents/SituationReport160208-001.pdf">Situation Report 001-Feb.8/2016 1030</a></li>
          <li><a href="/EMOSecureApp/Categories/157/documents/SituationReport160208-002.pdf">Situation Repor t002 Feb 8/2016 1900</a><a href="https://www.halifax.ca/EMOSecureApp/Categories/157/documents/SituationReport2016-001.pdf">.</a></li>
          <li><a href="/EMOSecureApp/Categories/157/documents/SituationReport160208-003.pdf">Situation Report 003 Feb 9/2016 0930</a></li>
        </ul>
        </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Logs" id="Logs"></a>Logs</td>
  </tr>
  <tr>
        <td>
        <ul>
          <li>No logs to report.</li>
        </ul>
        </td>
    </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Contacts" id="Contacts"></a>Contacts</td>
  </tr>
  <tr>
        <td>
        <ul>
      		<li><a href="" target="_blank">BPIM </a> - 20 KB [pdf]</li>
        </ul>
        </td>
    </tr>
</table>
