
<h3><? echo pageTitle(55); ?></h3>
<p style="margin-bottom: 0;">Click a link below to jump to that section and get the latest documents.</p>

<p style="margin-top: 0; margin-bottom: 0;">&nbsp;</p>
<table class="DocLinks">
    <tr>
        <td>
            <ul>
                <li><a href="#Updates">Updates</a></li>
                <li><a href="#Reports">Reports</a></li>
          	</ul>
        </td>
        <td>
            <ul>
				<li><a href="#Logs">Logs</a></li>
				<li><a href="#Contacts">Contacts &amp; Lists </a></li>
          	</ul>
        </td>
    </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Updates" id="Updates"></a>Updates</td>
  </tr>
  <tr>
        <td>
        <ul>      
          <li>No updates to report.</li>
        </ul>
        </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Reports" id="Reports"></a>Reports</td>
  </tr>
  <tr>
        <td>
        <ul>
          <li>No situations to report.</li>
        </ul>
        </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Logs" id="Logs"></a>Logs</td>
  </tr>
  <tr>
        <td>
        <ul>
          <li>No logs to report.</li>
        </ul>
        </td>
  </tr>
</table>



<table width="218" class="DocumentListings">
  <tr class="Title">
		<td><a name="Contacts" id="Contacts"></a>Contacts &amp; Lists </td>
  </tr>
  <tr>
        <td>
        <ul>
   		  <li><a href="/EMOSecureApp/Categories/55/Documents/ALERTLISTJanuary2012.pdf">Alert List</a>&nbsp; (January 2012) - [pdf]</li>
          <li><a href="https://www.halifax.ca/EMOSecureApp/Categories/55/Documents/PhoneDataPlanBackUp.pdf">Phone Data Back Up Plan </a>[pdf] </li>
          <li><a href="https://www.halifax.ca/EMOSecureApp/Categories/55/Documents/FallRiverBackUpEOC.pdf">Fall River Back Up EOC </a>[pdf]</li>
          <li>EOC wireless password VE1HRMEMO </li>
        </ul>
        </td>
  </tr>
</table>
