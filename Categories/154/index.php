<h3><?= pageTitle(154) ?></h3>
<p>Click a link below to jump to that section and get the latest documents.</p>

<table width="382" height="82" class="DocLinks">
  <tr>
    <td width="155">
      <ul>
        <li><a href="#Updates">Updates</a></li>
        <li><a href="#Reports">Reports</a></li>
        <li>Ops Briefings</li>
   	  </ul>
    </td>
    <td width="215">
      <ul>
	    <li><a href="#Logs">Logs</a></li>
	    <li><a href="#Contacts">Contacts</a></li>
	    <li><a href="https://www.halifax.ca/EMOSecureApp/Categories/154/TestPage.php">Start Memos</a></li>
	    <li>Media Reports</li>
	    <li>Links</li>
      </ul>
    </td>
  </tr>
</table>
<table class="DocumentListings">
  <tr class="Title">
	<td><a name="Updates" id="Updates"></a>Updates</td>
  </tr>
  <tr>
    <td>
      <ul>      
        <li><a href="" target="_blank">Update 1</a> - 20 KB [pdf]</li>
        <li><a href="" target="_blank">Update 2</a> - 25 KB [jpg]</li>
        <li><a href="" target="_blank">Update 3</a> - 10 KB [gif]</li>
      </ul>
    </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
	<td><a name="Reports" id="Reports"></a>Reports</td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>No situations to report.</li>
      </ul>
    </td>
  </tr>
</table>
<table width="158" border="1">
  <tr align="undefined" valign="top" bgcolor="#FFFFFF">
    <th width="161" scope="row"><font color="#000000">
    <div align="left"><a name="Reports" id="Reports2"></a>Ops Briefings</div>
    </font></th>
  </tr>
  <tr valign="top" bgcolor="#FFFFFF">
    <th align="undefined" scope="row">&nbsp;</th>
</table>
<table class="DocumentListings">
  <tr class="Title">
	<td><strong><a name="Logs" id="Logs"></a>Logs</strong></td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>No logs to report.</li>
      </ul>
    </td>
  </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
	<td><a name="Contacts" id="Contacts"></a><strong>Contacts</strong></td>
  </tr>
  <tr>
    <td>
      <ul>
   		<li><a href="" target="_blank">BPIM </a> - 20 KB [pdf]</li>
      </ul>
    </td>
  </tr>
</table>
<table width="163" border="1">
  <tr align="undefined" valign="top" bgcolor="#FFFFFF">
    <th width="153" scope="row"><font color="#000033">
      <div align="left"><a name="Reports" id="Reports3"></a>Staff Memos</div>
    </font></th>
  </tr>
  <tr valign="top" bgcolor="#FFFFFF">
    <th align="undefined" scope="row">&nbsp;</th>
</table>
<table width="163" border="1">
  <tr valign="top">
    <th width="153" scope="row"><div align="left"><a name="Reports" id="Reports4"></a>Media Reports</div></th>
  </tr>
  <tr valign="top">
    <th scope="row">&nbsp;</th>
</table>
<table width="163" border="1">
  <tr valign="top">
    <th width="153" scope="row"><div align="left"><a name="Reports" id="Reports5"></a>Links</div></th>
  </tr>
  <tr valign="top">
    <th scope="row">&nbsp;</th>
</table>
<p>&nbsp;</p>
