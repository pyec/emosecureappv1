 <?php
  /*---------------------------------------------------------------
   * 
   * 	MODULE:		intro.php
   * 	AUTHOR:		Gian
   * 	Created:	Unknown
   * 
   * 	This shows the introduction paragraph that users see
   * 	on login.
   * 
   * --------------------------------------------------------------
   * 
   * 	
   * 	MODIFICATION HISTORY
   * 	20170519	PRSC	Cleanup and PHP tabs fixed.
   *    * 
   *---------------------------------------------------------------
   */
?>
<h3>Welcome to the EMO Doc Share</h3>

<p style="padding-bottom:7px;">This document share has been developed to keep people who need to know, in the know, by providing a centralized place to post updates, information, and documentation.</p>

<p style="padding-bottom:7px;">This application manages multiple users, and multiple events, and users can be granted access to one or more events at a time.</p>

<p style="padding-bottom:7px;">Users can access this document share via desktops or laptop computers or smart mobile devices such as the iPhones and Blackberries.</p>

<p style="padding-bottom:7px;">If you have questions related to this application or the content herein, please contact: <a href="mailto:HRM_EMO@halifax.ca">HRM_EMO@halifax.ca</a>.</p>


<?php 

 if( $_SESSION['EMOUser']['UserGroup'] < 3 ) {

//	echo '<div id="WebServicsOnCall" style="border:none;border-top:1px solid #999;">';
	include ( $_SERVER['DOCUMENT_ROOT']."/EMOSecureApp/System/Includes/WebServicesOnCall.php" );
// 	echo '</div>';

 }

 ?>