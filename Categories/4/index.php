
<h3><? echo pageTitle(4); ?></h3>
<p>Click a link below to jump to that section and get the latest documents.</p>

<table width="414" class="DocLinks">
    <tr>
        <td width="225" valign="top">
            <ul>
                <li><a href="#Updates">Situation Reports </a></li>
                <li><a href="#Reports">Absenteeism</a></li>
          	    <li><a href="#Planning">Infectious Disease Planning</a> </li>
                <li><a href="#Video">Video</a></li>
                <li><a href="#BUPPE">Business Unit PPE Estimates</a> </li>
            </ul>
        </td>
        <td width="177">
            <ul>
				<li><a href="#Interest">Documents of Interest</a></li>
				<li><a href="#Contacts">HRM Manager Information</a></li>
          	    <li><a href="#Minutes">Minutes</a></li>
                <li><a href="#Links">Links of Interest</a></li>
            </ul>
        </td>
    </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Updates" id="Updates"></a>Situation Reports </td>
  </tr>
  <tr>
        <td>
        <ul>      
          <li><a href="/EMOSecureApp/Categories/4/Documents/SRIDRT001.pdf">November 5, 2009</a></li>
          <li><a href="/EMOSecureApp/Categories/4/Documents/SRIDRT-002.pdf">November 10, 2009</a></li>
          <li><a href="http://www.halifax.ca/EMOSecureApp/Categories/4/Documents/SRIDRT-003.pdf">November 13, 2009</a></li>
          <li><a href="/EMOSecureApp/Categories/4/Documents/SRIDRT-003.pdf">November 19, 2009</a> </li>
        </ul>
        </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Absenteeism" id="Reports"></a>Absenteeism</td>
  </tr>
  <tr>
        <td>
        <ul>
          <li>Report for<a href="/EMOSecureApp/Categories/4/Documents/Absenteeism0911Nov.pdf"> November</a></li>
          <li>Report for <a href="/EMOSecureApp/Categories/4/Documents/Absenteeism0910Oct.pdf">October</a>.</li>
        </ul>
        </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
		<td width="244"><a name="Planning" id="Logs"></a>Infectious Disease Planning</td>
  </tr>
  <tr>
        <td>
          <p><em><strong>Draft</strong></em> - <a href="/EMOSecureApp/Categories/4/Documents/DraftInfectiousDiseasePlan.pdf">Infectious Disease Plan </a></p>
          <p>Infectious Disease Planning<br> 
          Guidance Document </p>
          <ul>
            <li><a href="/EMOSecureApp/Categories/4/Documents/BPIM.pdf">BPIM</a></li>
            <li><a href="/EMOSecureApp/Categories/4/Documents/CAO.pdf">CAO</a></li>
            <li><a href="/EMOSecureApp/Categories/4/Documents/Finance.pdf">Finance</a></li>
            <li><a href="/EMOSecureApp/Categories/4/Documents/HRFE.pdf">Fire and Emergency</a></li>
            <li><a href="/EMOSecureApp/Categories/4/Documents/HR.pdf">Human Resources</a></li>
            <li><a href="/EMOSecureApp/Categories/4/Documents/IAM.pdf">IAM</a></li>
            <li><a href="/EMOSecureApp/Categories/4/Documents/IES.pdf">IES</a></li>
            <li><a href="/EMOSecureApp/Categories/4/Documents/Legal.pdf">Legal</a></li>
            <li><a href="/EMOSecureApp/Categories/4/Documents/MetroTransit.pdf">Halifax Transit</a></li>
            <li><a href="/EMOSecureApp/Categories/4/Documents/HRP.pdf">Police</a></li>
            <li><a href="/EMOSecureApp/Categories/4/Documents/TPW.pdf">TPW</a></li>
            <li><a href="/EMOSecureApp/Categories/4/Documents/HalifaxWaterPandemicFlu.pdf">Halifax Water</a></li>
            <li><a href="/EMOSecureApp/Categories/4/Documents/CommunityDevelopment.pdf">Community Development</a></li>
            <li><a href="/EMOSecureApp/Categories/4/Documents/CommunityDevelopment.pdf">Library</a></li>
          </ul></td>
  </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Video" id="Contacts"></a>Video</td>
  </tr>
  <tr>
        <td>
        <ul>
      		<li><a href="http://www.halifax.ca/EMOSecureApp/Categories/4/ThreePanelRespirator.html">Three Panel Respirator</a><br />
This video will show you the proper use and care of the 3M Filtering Facepiece Respirator... </li>
        </ul>
    </td>
  </tr>
</table>
<table class="DocumentListings">
  <tr class="Title">
    <td width="236"><a name="BUPPE" id="Contacts"></a>Business Unit PPE Estimates </td>
  </tr>
  <tr>
    <td><ul>
        <li><a href="/EMOSecureApp/Categories/4/Documents/HRP_EstimateFor_PPE.pdf">Halifax Regional Police</a></li>
        <li><a href="/EMOSecureApp/Categories/4/Documents/IAM_PPE_Assessment.pdf">IAM</a></li>
    </ul></td>
  </tr>
</table>
<table class="DocumentListings">
  <tr class="Title">
    <td width="230"><a name="Interest" id="Contacts"></a>Documents of Interest </td>
  </tr>
  <tr>
    <td><ul>
        <li><a href="/EMOSecureApp/Categories/4/documents/Documents/H1N1FastFacts.pdf" target="_blank">Fast facts for front line clinicians </a></li>
        <li><a href="/EMOSecureApp/Categories/4/documents/2009Oct01H1N1riskaxandPPEsummary.pdf">Risk Assessment &amp; PPE</a> </li>
        <li><a href="/EMOSecureApp/Categories/4/documents/ImmunizationProgramLtrDrStrangSept252009.pdf">Immunization Program Letter - Dr. Strang (Sept 25, 2009)</a> </li>
        <li><a href="/EMOSecureApp/Categories/4/documents/H1N1WorkplaceQAs.pdf">Workplace Q&amp;As </a></li>
        <li><a href="/EMOSecureApp/Categories/4/documents/ProtectYourselfPoster.pdf">H1N1 Poster </a></li>
        <li><a href="/EMOSecureApp/Categories/4/documents/Documents/H1N1Poster.pdf">H1N1 (HRM Internal) Poster </a></li>
        <li><a href="/EMOSecureApp/Categories/4/documents/H1N11stresponders.pdf">First Responder Guidelines </a></li>
        <li><a href="/EMOSecureApp/Categories/4/documents/AuditorGeneralReportJuly.pdf">Auditor General's Pandemic Report </a></li>
        <li><a href="/EMOSecureApp/Categories/4/documents/H1N1_1st_Responders_PPE.pdf">H1N1 1st Responders</a></li>
        <li><a href="/EMOSecureApp/Categories/4/documents/HRM_Pandemic_Supplies.pdf">HRM Pandemic Supplies</a> (June 3, 2009) </li>
        <li><a href="/EMOSecureApp/Categories/4/documents/H1N1Tracking.pdf">H1N1 Tracking</a></li>
        <li><a href="/EMOSecureApp/Categories/4/documents/pandemic-flu-lesson.pdf">Pandemic Flu Lesson </a></li>
        <li><a href="/EMOSecureApp/Categories/4/documents/HRMServiceCataloguebyBUJune2509.pdf">HRM Service Catalogue </a></li>
        <li><a href="/EMOSecureApp/Categories/4/documents/JULY6v1_PrehospitalCareclean.pdf">Prehospital Care </a></li>
        <li><a href="/EMOSecureApp/Categories/4/documents/H1N1Resources.pdf">Resources Announcement </a><br />
        </li>
        </ul></td>
  </tr>
</table>
<table class="DocumentListings">
  <tr class="Title">
    <td width="395"><a name="Managers" id="Contacts"></a>HRM Manager Information </td>
  </tr>
  <tr>
    <td><ul>
        <li><a href="/EMOSecureApp/Categories/4/Documents/H1N1GuideForManagers.pdf" target="_blank">Guide for Managers</a></li>
        <li><a href="/EMOSecureApp/Categories/4/Documents/BusinessPracticeForSickLeaveAdvance.pdf">Business Practice for Sick Leave Advance</a> </li>
    </ul></td>
  </tr>
</table>
<table class="DocumentListings">
  <tr class="Title">
    <td width="221"><a name="Minutes" id="Contacts"></a>Minutes</td>
  </tr>
  <tr>
    <td><ul>
        <li><a href="/EMOSecureApp/Categories/4/documents/InfectiousDiseaseCommitteeMinutes_Mar25_2009.pdf">March 25, 2009</a> </li>
        <li><a href="/EMOSecureApp/Categories/4/documents/InfectiousDiseaseCommitteeMinutes_May11_2009.pdf">May 11, 2009 </a></li>
        <li><a href="http://www.halifax.ca/EMOSecureApp/Categories/4/documents/InfectiousDiseaseCommitteeminutesJune1509.pdf">June 15, 2009</a> </li>
        <li><a href="/EMOSecureApp/Categories/4/documents/Infectious_Disease_Committee_minutes_July_2009.pdf">July 23, 2009 </a></li>
        <li><a href="/EMOSecureApp/Categories/4/documents/Infectious_Disease_Committee_minutes_August27_2009.pdf">August 27, 2009</a></li>
        <li><a href="/EMOSecureApp/Categories/4/documents/InfectiousDiseaseCommminutesSept2409.pdf">September 24, 2009</a> </li>
        <li><a href="/EMOSecureApp/Categories/4/documents/InfectiousDiseaseCommminutesOct2909.pdf">October 29, 2009</a> </li>
        <li><a href="/EMOSecureApp/Categories/4/documents/InfectiousDiseaseCommminutesDec309.pdf">December 3, 2009 </a></li>
        </ul></td>
  </tr>
</table>
<table width="433" class="DocumentListings">
  <tr class="Title">
    <td width="282"><a name="Links" id="Contacts"></a>Links of Interest </td>
  </tr>
  <tr>
    <td><ul>
        <li><a href="http://www.phac-aspc.gc.ca/alert-alerte/h1n1/vacc/pdf/vacc-eng.pdf" target="_blank">Vaccine Sequencing </a></li>
        <li><a href="http://www.cidrap.umn.edu/cidrap/content/influenza/swineflu/news/sep2109children-jw.html" target="_blank">Trial predicts 2 H1N1 shots for young kids, 1 for older</a></li>
        <li><a href="http://www.phac-aspc.gc.ca/alert-alerte/h1n1/hp-ps/cs-pc-eng.php" target="_blank">Interim Guidance: Prevention and management of cases of influenza-like-illness (ILI) that may be due to pandemic (H1N1) 2009 influenza virus on cruise ships</a></li>
        <li> <a href="http://www.phac-aspc.gc.ca/cpip-pclcpi/vf/index-eng.php)" target="_blank">Pandemic Vaccine Prioritization Framework</a></li>
        <li><a href="http://www.phac-aspc.gc.ca/alert-alerte/h1n1/hp-ps-info_health-sante-eng.php" target="_blank">Individual and Community Based Measures to Help Prevent Transmission of Influenza-Like-Illness (ILI) in the Community </a></li>
        <li><a href="http://www.phac-aspc.gc.ca/alert-alerte/h1n1/phg-ldp-eng.php" target="_blank">Public Health Guidance for the prevention and management of Influenza-like-illness (ILI), including the Pandemic (H1N1) 2009 Influenza Virus, related to mass gatherings</a></li>
        <li><a href="http://www.phac-aspc.gc.ca/alert-alerte/h1n1/vacc/vacc-eng.php" target="_blank">Guidance on H1N1 Vaccine Sequencing</a>            </li>
        </ul></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="margin-bottom: 0;">&nbsp;</p>
