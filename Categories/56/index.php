
<h3><? echo pageTitle(56); ?></h3>
<p>Click a link below to jump to that section and get the latest documents<strong>.<br>
  The EOC is now closed 11:00 hrs</strong> </p>

<table class="DocLinks">
    <tr>
        <td valign="top">
            <ul>
                <li><a href="#Updates">Updates</a></li>
                <li><a href="#Reports">Reports</a></li>
          	    <li><a href="#OpsBriefings">Ops Briefings</a> </li>
            </ul>
        </td>
        <td>
            <ul>
				<li><a href="#Logs">Logs</a></li>
				<li><a href="#Contacts">Contacts</a></li>
          	    <li><a href="#Staff">Staff Memos</a> </li>
          	    <li><a href="#Media">Media Reports</a> </li>
          	    <li><a href="#Links">Links</a></li>
            </ul>
        </td>
    </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Updates" id="Updates"></a>Updates</td>
  </tr>
  <tr>
        <td>
        <ul>      
          <li>No Updates </li>
          <li>No Updates </li>
          <li>No Updates </li>
        </ul>
        </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
    <td><a name="OpsBriefings" id="Reports"></a>Ops Briefings </td>
  </tr>
  <tr>
    <td><ul>
        <li><a href="/EMOSecureApp/Categories/56/Documents/100903-10am-OpsBriefing.pdf">10/09/03 10:00 </a> </li>
    </ul></td>
  </tr>
</table>
<table width="322" class="DocumentListings">
  <tr class="Title">
		<td width="94"><a name="Reports" id="Reports"></a>Reports</td>
  </tr>
  <tr>
        <td>
        <ul>
          <li><a href="/EMOSecureApp/Categories/56/Documents/100904-SitReport1.html">Situation Report - Sept 4- 7:31 am </a></li>
          <li><a href="https://www.halifax.ca/EMOSecureApp/Categories/56/Documents/100904Sitreport2.html">Situation Report - Sept 4- 10:00 am</a></li>
          <li><a href="https://www.halifax.ca/EMOSecureApp/Categories/56/Documents/100904Sitereport3.html">Situation Report - Sept 4 -12:30 pm</a></li>
          <li><a href="../../../EMOSecureApp/Categories/56/Documents/100904SitReport4.html">Situation Report - Sept 4 -16:30 pm</a> </li>
          <li><a href="/EMOSecureApp/Categories/56/Documents/100904SitReport5.html">Situation Report - Sept 4 -18:30 pm</a></li>
          <li><a href="/EMOSecureApp/Categories/56/Documents/100904SitReport6.html">Situation Report - Sept 4 -19:30 pm</a></li>
          <li><a href="/EMOSecureApp/Categories/56/Documents/100905SitReport7.html">Situation Report - Sept 5 -11:00 am</a> </li>
        </ul>
    </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Logs" id="Logs"></a>Logs</td>
  </tr>
  <tr>
        <td>
        <ul>
          <li>No logs to report.</li>
        </ul>
        </td>
  </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Contacts" id="Contacts"></a>Contacts</td>
  </tr>
  <tr>
        <td>
        <ul>
      		<li>To be updated </li>
        </ul>
        </td>
  </tr>
</table>
<table class="DocumentListings">
  <tr class="Title">
    <td width="228"><a name="Staff" id="Contacts"></a>Staff Memo's </td>
  </tr>
  <tr>
    <td><ul>
        <li><a href="https://www.halifax.ca/EMOSecureApp/Categories/56/Documents/100902-StaffMemo-1.pdf">Staff Memo 1 11:39 am (pdf)</a></li>
        <li><a href="../../../EMOSecureApp/Categories/56/100903-StaffMemo-1.html">Staff Memo 1 11:39- html </a></li>
    </ul></td>
  </tr>
</table>
<table class="DocumentListings">
  <tr class="Title">
    <td><a name="Media" id="Contacts"></a>Media Reports </td>
  </tr>
  <tr>
    <td><ul>
        <li><a href="/EMOSecureApp/Categories/56/Documents/100902-MediaRelease.pdf">10/09/02 - 1 </a></li>
    </ul></td>
  </tr>
</table>
<table class="DocumentListings">
  <tr class="Title">
    <td><a name="Links" id="Contacts"></a>Links</td>
  </tr>
  <tr>
    <td valign="top"><ul>
        <li><a href="http://www.weatheroffice.gc.ca/hurricane/track_e.html" target="_blank">Canadian Hurricane Center </a>- <strong>Track</strong></li>
        <li><a href="http://www.weatheroffice.gc.ca/hurricane/statements_e.html" target="_blank">Canadian Hurricane Center </a> - <strong>Statements</strong> </li>
    </ul></td>
  </tr>
</table>
<p>&nbsp;</p>
