
<h3><? echo pageTitle(2); ?></h3>
<p>Click a link below to jump to that section and get the latest documents.</p>

<table class="DocLinks">
    <tr>
        <td>
            <ul>
                <li><a href="#Updates">Meetings</a></li>
                <li><a href="#Docs">Docs of Interest</a></li>
          	</ul>
        </td>
        <td>
            <ul>
				<li><a href="#Plans">Plans/Templates</a></li>
				<li><a href="#Contacts">Contact Info</a></li>
          	    <li><a href="#Links">Links of Interest </a></li>
            </ul>
        </td>
    </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Meetings" id="Updates"></a>Meetings</td>
  </tr>
  <tr>
        <td><ul><li><a href="http://www.halifax.ca/emosecureapp/Categories/2/Documents/BusinessContinuityMinutesApr132010.pdf">April 13, 2010</a> <a href="/site/pdftips.php" target="_blank"><img src="/images/pdf_image.gif" width="20" height="20" border="0" alt="To PDF Acrobat Tips"></a></li>
          <li><a href="Categories/2/Documents/BCPCommitteeMinutes18May2010.pdf">May 18, 2010</a>&nbsp; <a href="/site/pdftips.php" target="_blank"><img src="/images/pdf_image.gif" width="20" height="20" border="0" alt="To PDF Acrobat Tips"></a></li>
          <li>August 26, 2010  Eric Spicer Bldg 2:00-3:00 PM </li>
        </ul>
    </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Docs" id="Reports"></a>Documents of Interest </td>
  </tr>
  <tr>
    <td>
        <ul>
          <li><a href="Categories/2/Documents/HazardsAnalysisHRM.pdf">Hazard Analysis 1990 </a> <a href="/site/pdftips.php" target="_blank"><img src="/images/pdf_image.gif" width="20" height="20" border="0" alt="To PDF Acrobat Tips"></a></li>
          <li><a href="Categories/2/Documents/HazardsAnalyses1996B3022.pdf">Hazard Analysis 1996</a> <a href="/site/pdftips.php" target="_blank"><img src="/images/pdf_image.gif" width="20" height="20" border="0" alt="To PDF Acrobat Tips"></a></li>
        </ul>
    </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Plans" id="Logs"></a>Plans/Templates</td>
  </tr>
  <tr>
        <td>
        <ul>
          <li><a href="Categories/2/Documents/BCPBUTemplatev12.pdf">Business Unit Template</a> <a href="/site/pdftips.php" target="_blank"><img src="/images/pdf_image.gif" width="20" height="20" border="0" alt="To PDF Acrobat Tips"></a></li>
        </ul>
    </td>
  </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Contacts" id="Contacts"></a>Contact Info </td>
  </tr>
  <tr>
        <td>
        <ul>
      		<li><a href="mailto:manuelb@halifax.ca">Barry Manual &nbsp; 490-4213</a>  </li>
            <li><a href="mailto:larabit@halifax.ca%20">Tammy Larabie&nbsp; 490-3573</a></li>
            <li><a href="Categories/2/Documents/BCPMembership032010.pdf">BCP Membership List&nbsp; </a><a href="/site/pdftips.php" target="_blank"><img src="/images/pdf_image.gif" width="20" height="20" border="0" alt="To PDF Acrobat Tips"></a></li>
        </ul>
    </td>
  </tr>
</table>
<table class="DocumentListings">
  <tr class="Title">
    <td><a name="Links" id="Contacts"></a>Links of Interest </td>
  </tr>
  <tr>
    <td><ul>
      <li><a href="http://insidehrm/BusinessUnits/ServiceCatalogue/index.html" target="_blank">insidehrm Service Catalogue</a></li>
    </ul></td>
  </tr>
</table>
<p style="margin-bottom: 0;">&nbsp;</p>
