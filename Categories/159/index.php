<h3><?= pageTitle(159) ?></h3>
<p>Click a link below to jump to that section and get the latest documents.</p>

<table class="DocLinks">
    <tr>
        <td width="101">
            <ul>
                <li><a href="#Updates">Updates</a>- <a href="https://www.halifax.ca/EMOSecureApp/Categories/159/images/Plumearea.png">project plume</a></li>
              <li><a href="#Reports">Reports</a></li>
       	  </ul>
        </td>
        <td width="96">
            <ul>
				<li><a href="#Logs">Logs</a></li>
				<li><a href="#Contacts">Contacts</a></li>
          	</ul>
        </td>
    </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Updates" id="Updates"></a>Updates</td>
  </tr>
  <tr>
        <td>
        <ul>      
          <li><a href="https://www.halifax.ca/EMOSecureApp/Categories/159/documents/SituationReport1.docx" target="_blank">Sitrep1</a></li>
          <li><a href="https://www.halifax.ca/EMOSecureApp/Categories/159/documents/SituationReport2.docx" target="_blank">Sitrep2</a></li>
          <li><a href="https://www.halifax.ca/EMOSecureApp/Categories/159/documents/SituationReport3.docx">Sitrep3</a></li>
          <li><a href="https://www.halifax.ca/EMOSecureApp/Categories/159/documents/SituationReport4.docx">Sitrep4</a></li>
          <li><a href="https://www.halifax.ca/EMOSecureApp/Categories/159/documents/HotWash.docx">ENDEX</a></li>
        </ul>
        </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Reports" id="Reports"></a>Reports</td>
  </tr>
  <tr>
        <td>
        <ul>
          <li>No situations to report.</li>
        </ul>
        </td>
    </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Logs" id="Logs"></a>Logs</td>
  </tr>
  <tr>
        <td>
        <ul>
          <li>No logs to report.</li>
        </ul>
        </td>
    </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Contacts" id="Contacts"></a>Contacts</td>
  </tr>
  <tr>
        <td>
        <ul>
      		<li><a href="" target="_blank">BPIM </a> - 20 KB [pdf]</li>
        </ul>
        </td>
    </tr>
</table>
