<h3><?= pageTitle(160) ?></h3>
<p>Click a link below to jump to that section and get the latest documents.</p>

<table width="344" height="85" class="DocLinks">
  <tr>
    <td width="171">
      <ul>
        <li><a href="#Updates">Exercise King Fisher</a></li>
        <li><a href="#Logs">Blank 2</a></li>
      </ul>
    </td>
    <td width="161">
      <ul>
		<li><a href="#reports"> Miscellaneous</a></li>
		<li><a href="#Contacts">Blank 3</a></li>
   	  </ul>
    </td>
  </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
	<td width="292"><a name="Updates" id="Updates"></a>Exercise Kingfisher</td>
  </tr>
  <tr>
    <td>
      <ul>      
        <li><a href="/EMOSecureApp/Categories/160/documents/ExerciseKingfisherFinalReport-Complete.pdf">Final Report</a></li>
        <li><a href="" target="_blank">Kingfisher Recomdendations</a></li>
        <li><a href="/EMOSecureApp/Categories/160/documents/KingfisherRecommendationsExtractedV1.pdf">Kingfisher Recommendations Extracted</a></li>
      </ul>
    </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
	<td><a name="Reports" id="Reports"></a> Miscellaneous</td>
  </tr>
  <tr>
    <td>
      <ul>
        <li> <strong><a href="/EMOSecureApp/Categories/160/documents/HamRadiototheRescue.pdf">Ham Radio to the  Rescue</a></strong></li>
        <li><strong><a href="/EMOSecureApp/Categories/160/documents/HospitalCallsigns.pdf">Hospital Call Signs</a></strong></li>
      </ul>
    </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
	<td width="208"><a name="Logs" id="Logs"></a> SOGs</td>
  </tr>
  <tr>
    <td>
      <ul>
        <li><a href="/EMOSecureApp/Categories/160/documents/MESSAGEHANDLINGGUIDERev1.3.12016.pdf">Message Handling Guide</a></li>
      </ul>
    </td>
  </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
	<td><a name="Contacts" id="Contacts"></a>Blank 3</td>
  </tr>
  <tr>
    <td>
      <ul>
   		<li></li>
      </ul>
    </td>
  </tr>
</table>
