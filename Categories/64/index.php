
<h3><? echo pageTitle(64); ?></h3>
<p style="margin-bottom: 0;">Click a link below to jump to that section and get the latest documents.</p>

<p style="margin-top: 0; margin-bottom: 0;">&nbsp;</p>
<table class="DocLinks">
    <tr>
        <td>
            <ul>
                <li><a href="#Updates">Post Exercise Reports</a></li>
                <li><a href="#Reports">Templates</a></li>
          	</ul>
        </td>
        <td>
            <ul>
				<li><a href="#Logs">Support Documentation</a></li>
			  <li><a href="#Contacts">Contacts</a></li>
       	  </ul>
        </td>
    </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Updates" id="Updates"></a>Post Exercise Reports (PXR)</td>
  </tr>
  <tr>
        <td>
        <ul>      
          <li><a href="https://www.halifax.ca/emosecureapp/Categories/64/documents/PXR-130406JEMConnect.pdf">PXR -130406 JEM Connect<a></a></a> - 86 kb (pdf)</li>
          <li><a href="" target="_blank">Update 2</a> - 25 KB [jpg]</li>
          <li><a href="" target="_blank">Update 3</a> - 10 KB [gif]</li>
        </ul>
        </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Reports" id="Reports"></a>Templates</td>
  </tr>
  <tr>
        <td>
        <ul>
          <li><a href="/emosecureapp/Categories/64/documents/00GenericExercisePlan-content.pdf">Generic Exercise Plan - Content&nbsp; </a>76 kb (pdf)</li>
          <li><a href="/emosecureapp/Categories/64/documents/ExercisePlan-short.docx">Generic Exercise Plan - Short<a></a></a> 16&nbsp;kb (docx)</li>
          <li><a href="/emosecureapp/Categories/64/documents/00ExercisePlan-00blankv1-1.docx">Generic Exercise Plan - Full</a>&nbsp; 46 kb (docx)</li>
          <li><a href="/emosecureapp/Categories/64/documents/00-PXR130000.docx">Post Exercise Report - Blank&nbsp; <a></a></a>91 kb (docx)</li>
        </ul>
        </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Logs" id="Logs"></a>Support Documentation</td>
  </tr>
  <tr>
        <td>
        <ul>
          <li><a href="/emosecureapp/Categories/64/documents/Bi-SCEXERCISEDIRECTIVE75-323December2008.pdf">Bi-SC EXERCISE DIRECTIVE 75-3 23 December 2008<a></a></a> 1,975 kb (pdf)</li>
        </ul>
        </td>
  </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Contacts" id="Contacts"></a>Contacts</td>
  </tr>
  <tr>
        <td>
        <ul>
      		<li><a href="" target="_blank">BPIM </a> - 20 KB [pdf]</li>
        </ul>
        </td>
    </tr>
</table>
