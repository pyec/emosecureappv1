
<h3><? echo pageTitle(53); ?></h3>
<p><br>Click a link below to jump to that section and get the latest documents.</p>
<table class="DocLinks">
    <tr>
        <td width="93" valign="top">
            <ul>
                <li><a href="#Updates">Updates</a></li>
                <li><a href="#Reports">Reports</a></li>
          	</ul>
        </td>
        <td width="166">
            <ul>
				<li><a href="#Logs">Logs</a></li>
				<li><a href="#Contacts">Contacts</a></li>
				<li><a href="#OldEvents">Old Events</a> </li>
          	</ul>
        </td>
    </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Updates" id="Updates"></a>Updates</td>
  </tr>
  <tr>
        <td>
        <ul>      
          <li><a href="" target="_blank">Update 1</a> - 20 KB [pdf]</li>
          <li><a href="" target="_blank">Update 2</a> - 25 KB [jpg]</li>
          <li><a href="" target="_blank">Update 3</a> - 10 KB [gif]</li>
        </ul>
        </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Reports" id="Reports"></a>Reports</td>
  </tr>
  <tr>
        <td>
        <ul>
          <li>No situations to report.</li>
        </ul>
        </td>
  </tr>
</table>




<table class="DocumentListings">
  <tr class="Title">
		<td><a name="Logs" id="Logs"></a>Logs</td>
  </tr>
  <tr>
        <td>
        <ul>
          <li>No logs to report.</li>
        </ul>
        </td>
  </tr>
</table>



<table class="DocumentListings">
  <tr class="Title">
    <td width="249"><a name="Contacts" id="Logs"></a>Contacts</td>
  </tr>
  <tr>
    <td><ul>
        <li><a href="https://www.halifax.ca/EMOSecureApp/Categories/56/Documents/EMO-OperationsOfficers.pdf">Operations Officers</a> - Sept 2010 .</li>
    </ul></td>
  </tr>
</table>
<table class="DocumentListings">
  <tr class="Title">
		<td width="243"><a name="OldEvents" id="Contacts"></a>Old Events </td>
  </tr>
  <tr>
        <td>
        <ul>
      		<li><a href="http://www.halifax.ca/emosecureapp/categories/53/TropicalStormDannyAugust282009.html">Tropical Storm Danny<br>
   		    August 28, 2009</a> </li>
        </ul>
    </td>
  </tr>
</table>
