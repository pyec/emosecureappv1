<?



			
		function dbConnect() 
		{
			global $conn;

			$dbHost = "localhost";
			$dbUser = "EMO_secure";
			$dbPass = "3m053cur3";
			$dbName = "EMODocExchange";
			
			$conn = mssql_connect ( $dbHost, $dbUser, $dbPass ) or die ( 'Can not connect to server' );
			mssql_select_db ( $dbName, $conn ) or die ( 'Can not select database' );

		}
		
		
		// List Category Checkboxes
		function listCategoryCheckboxes() 
		{
			global $conn;
			dbConnect();
	
			// SQL
			$sql = "SELECT * FROM Categories";
			$rs = mssql_query ( $sql, $conn ) or die ( 'Query Error' );

			// Loop record set
			while ( $row = mssql_fetch_array ( $rs ) )
			{
				echo '<span style="padding-bottom:5px;display:block">'
					.'<input class="CheckBox" type="checkbox" name="'.$row["Name"].'" id="'.$row["Name"].'" value="'.$row["CID"].'" />'
					.'<label for="'.$row["Name"].'">('.$row["CID"].') '.$row["Name"].'</label>'
					.'</span>';
			}
	
			mssql_close ( $conn );

		}
		
		// List Categories
		function listCategoryDropDownOptions() 
		{

			$sql = "SELECT * FROM Categories";
	
			$output = '<form style="display:inline;float:right;"><select>'
					 .'<option value="" selected>-All-</option>';

			global $conn;
			dbConnect();
			$rs = mssql_query ( $sql, $conn ) or die ( 'Query Error' );

			// Loop record set
			while ( $row = mssql_fetch_array ( $rs ) )
			{
				$output .= '<option value="'.$row["CID"].'">'.$row["Name"].'</option>';
			}
			mssql_close ( $conn );
		
			$output .= '</select></form>';
			
			return $output;
	
		}
		

		// List Category Admin - Not in use.
		function listCategoryAdmin() 
		{

			// SQL
			$sql = "SELECT * FROM Categories";

			$output = '<form>'
					.'<table class="UserListing">'
					.'<tr><td class="Title MenuDivider">Edit Categories</td></tr>';

			global $conn;
			dbConnect();
			$rs = mssql_query ( $sql, $conn ) or die ( 'Query Error' );

			// Loop record set
			while ( $row = mssql_fetch_array ( $rs ) )
			{
				$output .= '<tr><td>'.$row["CID"].' <input class="Categories" name="f'.$row["CID"].'" type="text" id="f'.$row["CID"].'" value="'.$row["Name"].'" /></td></tr>';
			}
	
			mssql_close ( $conn );
			
					
			$output .= '<tr><td><input class="Button" type="submit" value="Update" name="submitform" /></td></tr></table></form>';
					
			$output .= '<br /><br />'
					.'<form>'
					.'<table class="UserListing">'
					.'<tr><td class="Title MenuDivider" colspan="2">Add New Category</td></tr>'
					.'<tr><td class="Title">Name</td><td><input class="Categories" name="fCategoryName" type="text" id="fCategoryName" value="" /></td></tr>'
					.'<tr><td class="Title">URL</td><td><input class="Categories" name="fCategoryURL" type="text" id="fCategoryURL" value="" /></td></tr>'
					.'<tr><td colspan="2"><input class="Button" type="submit" value="Add" name="submitform" /></td></tr></table></form>';
					
			echo $output;

		}	


		// List Users
		function listUsers()
		{

			$StatusMsg = "No status to display";
			$categoryDropDownMenu = listCategoryDropDownOptions();
			$sql = "SELECT * FROM Users";
		
			// Create header
			$output = '<table class="UserListing">'
					.'<tr><td class="Title">'
					.$categoryDropDownMenu
					.'Users</td></tr>';

			// Loop record set
			$count = 0;
			$emailAddresses = "";

			global $conn;
			dbConnect();
			$rs = mssql_query ( $sql, $conn ) or die ( 'Query Error' );
			
			while ( $row = mssql_fetch_array ( $rs ) )
			{

				$output .= '<tr>'
						.'<td class="User" title="'.$row["FirstName"].' '.$row["LastName"].'">'
						.'<span style="float:right;">'
						.'<a href="mailto:'.$row["Email"].'?subject=EMO Document Share">Email</a> � '
						.'<a href="">Reset</a> � '
						.'<a href="Includes/Process.php?x=1&id='.$row["UID"].'" onclick="javascript:ConfirmDelete(this,\''.$row["FirstName"].' '.$row["LastName"].'\');">Delete</a>'
						.'</span>'
						.'<a href="user.php?id='.$row["UID"].'">'.$row["FirstName"].' '.$row["LastName"].'</a> '
						.'</td>'
						.'</tr>';
					
				$count++;
				
				if($emailAddresses == "") 
					$emailAddresses = $row["Email"];
				else
					$emailAddresses .= ','.$row["Email"];

			}
			
			mssql_close ( $conn );
					
			$output .= '<tr><td class="Title">'
					.'<span style="float:right"><a href="">Reset all users</a></span>'
					.'<a href="mailto:'.$emailAddresses.'?subject=EMO Document Share">Email all users</a>'
					.'</td></tr>'
					.'</table>';
					
			echo $output;
	
		}	


		// List Menu Items
		function listMenuItems() 
		{
			global $conn;
			dbConnect();
	
			// SQL
			$sql = "SELECT * FROM Categories";
			$rs = mssql_query ( $sql, $conn ) or die ( 'Query Error' );

			// Loop record set
			while ( $row = mssql_fetch_array ( $rs ) )
			{
				echo '<li><a href="'.$row["URL"].'">'.$row["Name"].'</a></li>';
			}
	
			mssql_close ( $conn );

		}	





		// Template... 		
		function functionName() 
		{
			global $conn;
			dbConnect();
	
			// SQL
			$sql = "SELECT * FROM Categories";
			$rs = mssql_query ( $sql, $conn ) or die ( 'Query Error' );

			// Loop record set
			while ( $row = mssql_fetch_array ( $rs ) )
			{

			}
	
			mssql_close ( $conn );

		}	
		
		
		


function CheckVars($x) {
	
	global $ErrorMsg;
	global $AppTitle;

	$ErrorMsg = "<tr><td class='ErrorMsg' colspan='2'>";

	switch($x) {
	
		case '1':
			$ErrorMsg .= "You must be logged in to see the ".$AppTitle; 
			break;
			
		case '2':
			$ErrorMsg .= "Invalid login credentials";
			break;

		case '3':
			$ErrorMsg .= "Invalid password";
			break;
			
		case '4':
			$ErrorMsg .= "You have been logged out.";
			break;
			
		case '5':
			$ErrorMsg .= "Login credentials have been sent to your email.";
			break;
			
		case '6':
			$ErrorMsg .= "Email address not found in our records.";
			break;

	}
	
	$ErrorMsg .= "</td></tr>";

	return $ErrorMsg;	

}


function ManageUsers($x) {

	// Connect to the database
	global $db;
	ConnectDB();

	// SQL Query to run
	$SQL = "SELECT * FROM EMO_User ORDER BY FirstName ASC, LastName ASC";
			  
	// Run it
	$rs = $db->Execute($SQL);
	
	// If records exist
	if ($rs->RowCount()){
	
		// Create headers
		echo "<table class='Users'>"
			."<tr>\n"
			."<td class='Title'>Users</td>\n"
			."</tr>\n";
			
		if($_REQUEST['msg']) {
		
			$StatusMsg = $_REQUEST['msg'];
			
			echo "<tr>\n"
				."<td colspan='2' class='ErrorMsg'>".$StatusMsg."</td>\n"
				."</tr>\n";
		
		}			
		
		// Count rows
		$i = 0;
		while($row = $rs->FetchRow()){
		
			//if($i%2 == 0)	$RowColor = "#F0F0F0";
			//else 			$RowColor = "#fcfcfc";
			
			//if(isset($x) && $x == $row['ID']) $RowColor = "#E8FECD";

			echo "<tr style='background:".$RowColor.";'>\n"
				."<td class='User' title='".$row['FirstName']." ".$row['LastName']."'>"
				."<a href='user.php?id=".$row['ID']."'>".$row['FirstName']." ".$row['LastName']."</a> "
				."<span style='float:right;'>"
				."<!--<a href=''>Send Login</a> - -->"
				."<a href='mailto:".$row['EmailAddress']."?subject=EMO Data Share'>Email</a> - "
				."<!-- <a href='AddModifyUsers.php?id=".$row['ID']."'>Edit</a> - -->"
				."<a href=''>Reset</a> - "
				."<a href='Includes/Process.php?x=1&id=".$row['ID']."' onclick='javascript:ConfirmDelete(this,\"".$row['FirstName']." ".$row['LastName']."\");'>Delete</a>"
				."</span>"
				."</td>\n"
				."</tr>\n";
				
			$i++;
		}
		
		echo "<tr><td>&nbsp;</td></tr><tr>\n"
			."<td><a href='#top'>Back to top</a></td>\n"
			."</tr>\n";
		
		echo "</table>\n";
		
	}
	
	$db->Close();

}

function GetUser($ID) {

	if (isset($ID)) {
	
		global $db;	
		ConnectDB();
		
		$SQL = "SELECT * FROM EMO_User WHERE ID = '".$ID."'";
					  
		// Run it
		$rs = $db->Execute($SQL);
	
		// If user exists return the record
		if ($rs->RowCount()) {
		
			$row = $rs->FetchRow();
			
			$Result = array($row['ID'],$row['FirstName'],$row['LastName'],$row['EmailAddress'],$row['GroupID'],$row['Login'],$row['Password']);
			
			return $Result;
			
		}
		
		$db->Close();
	
	}


}


function Login($Username,$Password) {

	// If the user isn't logged in try to autheticate
	if(empty($_SESSION['EMOUser']['ID'])) {
	
		// Continue only if a username and password are passed
		if(isset($Username) && isset($Password)) {

			// Connect to the database			
			global $db;	
			ConnectDB();
		
			// Strip username of illegal characters
			$IllegalCharacters = array("'", "=", ";", ":");
			$Username = str_replace($IllegalCharacters, "", $Username);
		
			// Prevent SQL Injection attempts by ensuring the username is less than eight characters
			$Username = substr($Username, 0, 8);
		
			// Show the modifed username
			// echo $Username."<br/>";
		
			// SQL Query to run
			$SQL = "SELECT * FROM EMO_User WHERE Login = '".$Username."'";
					  
			// Run it
			$rs = $db->Execute($SQL);
		
			// If user exists return the record
			if ($rs->RowCount()) {
				$row = $rs->FetchRow();
				
				// If the password doesn't match, redirect to login page with error
				if ($row['Password'] != $Password) {
					header('Location: index.php?x=3');
					//break;
				}
				
				// If user does exist and autheticates correctly, create a session

				$_SESSION['EMOUser']['ID'] 			= $row['ID']."\n";
				$_SESSION['EMOUser']['FirstName'] 	= $row['FirstName']."\n";
				$_SESSION['EMOUser']['LastName'] 	= $row['LastName']."\n";
				$_SESSION['EMOUser']['GroupID'] 	= $row['GroupID']."\n";
				$_SESSION['EMOUser']['Email'] 		= $row['EmailAddress']."\n";		
			
			// If that user does not exist, redirect to login page with appropriate error
			} else {
				header('Location: index.php?x=2');
				//break;
			}
		
			// Close database connection
			$db->Close();

		// If no login credentials are passed and no session is set, redirect to homepage with error
		} else {	
				header('Location: index.php?x=1');
			
		}
	}
	
}



function CheckLoginStatus() {
	if(empty($_SESSION['EMOUser']))
		header('Location: index.php?x=1');
}




function ShowPage($x) {

	if(isset($x)) {
		
		// Show Special Ops Page (only to those authorized)
		if($_SESSION['EMOUser']['GroupID'] == 4 && $x == 3) { 
			include($_SERVER['DOCUMENT_ROOT']."/EMOSecure/Includes/Ops-O.html");
		} 
		
		// Special Ops see from here down
		if($_SESSION['EMOUser']['GroupID'] >= 3 && $x == 2) { 
			include($_SERVER['DOCUMENT_ROOT']."/EMOSecure/Includes/Sit-Reps.html");
		} 
		
		// Special Ops see from here down
		if($_SESSION['EMOUser']['GroupID'] >= 2 && $x == 1) {
			include($_SERVER['DOCUMENT_ROOT']."/EMOSecure/Includes/General.html");	
		}
	
	} else {
		include($_SERVER['DOCUMENT_ROOT']."/EMOSecure/Includes/Common.html");
	}	

}




function userForgetPassword($email)
{
	global $db;
	ConnectDB();

	$sqlQuery = "SELECT * "
              . "FROM AppUser "
              . "WHERE (Email1 = '" . $email . "') OR (Email2 = '" . $email . "')";
	$rs = $db->Execute($sqlQuery);
	$row = $rs->FetchRow(); 
	
	//If Rows are Returned
	if($rs->RowCount())
	{
		$username 	= $row['Username'];
		$fname 		= $row['FirstName'];
		$lname 		= $row['LastName'];
		$active 	= $row['IsActive'];
		$password 	= base64_encode($row['Password']);
		
		//Pass the Users Information including the encrypted password to the form script located on the EServices Server.
		header("Location: http://eservices.halifax.ca/_private/hrpcalendar/form.php?e=$email&u=$username&fn=$fname&ln=$lname&p=$password&a=$active");
	}
	else //No Rows Returned
	{	
		//If No Rows are Returned the User entered invaild Information so kick them back to the Forgotten Password page
		header("Location: http://www.halifax.ca/Police/VolunteerCalendar/login.php?action=FORGOT&status=FAIL");	
	}
}


?>