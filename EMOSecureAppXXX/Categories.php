<?
	// Created by Gian Pompilio, HRM Web Services (September 24-09)
	// Contact pompilg@halifax.ca for information
 	
	// Load functions
	require_once( "init.php" );
	CheckLoginStatus(1);	// 1 = admins only


	if( isset( $_REQUEST['fState'] ) ) {

		global $conn;
		dbConnect();

		switch( $_REQUEST['fState'] ) {

			case "Update":

				// Assign new category access rights
				foreach( $_POST as $key => $value ) {
					if($key != "fState" && $key != "fState") {
							$sql = "UPDATE Categories SET Name = '".$value."' WHERE CID = '".substr( $key, 1 )."'";
							$rs = mssql_query( $sql, $conn ) or die( 'Error updating category information.' );
					}
				}

				break;


			case "Add":

				$sql = "INSERT INTO Categories (Name) VALUES('".$_POST['fCategoryName']."')";
				$rs = mssql_query( $sql, $conn ) or die( 'Error inserting new category.' );

				$sql = "SELECT * FROM Categories WHERE Name = '".$_POST['fCategoryName']."'";
				$rs = mssql_query( $sql, $conn ) or die( 'Error inserting new category.' );

				$row = mssql_fetch_array( $rs );

//				if( $row = mssql_fetch_array( $rs ) ) {			
//					echo $row["CID"].": ".$row["Name"];
//				}
				
				if( isset( $row["CID"] ) ) {
				
					mkdir( 'D:/HRM/Website/EMOSecureApp/Categories/'.$row["CID"] );
					$file = 'D:/HRM/Website/EMOSecureApp/Categories/Template.txt';
					$newfile = 'D:/HRM/Website/EMOSecureApp/Categories/'.$row["CID"].'/index.php';
	
					if (!copy($file, $newfile)) {
						echo "failed to copy $file...\n";
					}
	
	
					$handle = fopen( $newfile, 'r+' );
					$contents = fread( $handle, filesize( $newfile ) );
					$new_contents = str_replace( "%CatNum%", $row["CID"], $contents );
					fclose( $handle ); 
	
					$handle = fopen( $newfile, 'w' );
					fwrite( $handle, $new_contents );
					fclose( $handle ); 

				}
	
				break;


			case "Delete":
				
				//echo 'Delete '.$_REQUEST['cid'];

				//Remove from category table
				$sql = "DELETE FROM Categories WHERE CID = '".$_REQUEST['cid']."'";
				$rs = mssql_query( $sql, $conn ) or die( 'Error inserting new category.' );

				//Remove from user access table
				$sql = "DELETE FROM CategoryAccess WHERE CID = '".$_REQUEST['cid']."'";
				$rs = mssql_query( $sql, $conn ) or die( 'Error inserting new category.' );

				//Delete Category file from server
				//unlink('D:/HRM/Website/EMOSecureApp/Categories/'.$_REQUEST["cid"].'.html');
				//echo rmdir( 'D:/HRM/Website/EMOSecureApp/Categories/'.$_REQUEST['cid'] );
				//remove_dir( 'D:/HRM/Website/EMOSecureApp/Categories/'.$_REQUEST['cid'] );
				RemoveDirectory( 'D:/HRM/Website/EMOSecureApp/Categories/'.$_REQUEST['cid'] );

				break;


			case "Archive":
				
				$msg = '<a href="/EMOSecureApp/Categories/Archives/'.$_REQUEST["title"].'">'.$_REQUEST['title'].'</a> has been archived.';

				if( $row = mssql_fetch_array( $rs ) ) {			
					echo $row["CID"].": ".$row["Name"];
				}
				
				//smartCopy( 'D:/HRM/Website/EMOSecureApp/Categories/'.$_REQUEST["cid"], 'D:/HRM/Website/EMOSecureApp/Categories/Archives/'.$_REQUEST["cid"] );

				//rename('D:/HRM/Website/EMOSecureApp/Categories/'.$_REQUEST["cid"], 'D:/HRM/Website/EMOSecureApp/Categories/Archives/'.$_REQUEST["cid"]);

				if ( !rename( 'D:/HRM/Website/EMOSecureApp/Categories/'.$_REQUEST["cid"], 'D:/HRM/Website/EMOSecureApp/Categories/Archives/'.$_REQUEST["title"] ) ) {
					echo "failed to move directory...";
				}
				
				$newfile = 'D:/HRM/Website/EMOSecureApp/Categories/Archives/'.$_REQUEST["title"].'/index.php';
/*				
				$file = 'D:/HRM/Website/EMOSecureApp/Categories/'.$_REQUEST["cid"].'.html';
				$newfile = 'D:/HRM/Website/EMOSecureApp/Categories/Archives/'.$_REQUEST["cid"].'/'.$_REQUEST["title"].'.php';

				if (!copy($file, $newfile)) {
					echo "failed to copy $file...\n";
				}
				
*/

				$handle = fopen( $newfile, 'r+' );
				$contents = fread( $handle, filesize( $newfile ) );
				$title = "<? echo pageTitle(".$_REQUEST['cid']."); ?>";
				$new_contents = str_replace( $title, str_replace("-"," ",$_REQUEST["title"]), $contents );
				fclose( $handle ); 

				$handle = fopen( $newfile, 'w' );
				fwrite( $handle, $new_contents );
				fclose( $handle );

				$output = 'Categories.php?msg='.$msg;


				//Remove from category table
				$sql = "DELETE FROM Categories WHERE CID = '".$_REQUEST['cid']."'";
				$rs = mssql_query( $sql, $conn ) or die( 'Error inserting new category.' );

				//Remove from user access table
				$sql = "DELETE FROM CategoryAccess WHERE CID = '".$_REQUEST['cid']."'";
				$rs = mssql_query( $sql, $conn ) or die( 'Error inserting new category.' );

				break;		

		}

		mssql_close( $conn );

		if( isset( $output ) ) {
			header( 'Location: Categories.php?msg='.$msg );
		}

	}



	// Begin Assembling Page
	
	require_once ( $Server.$SysRootInc."Header.php" );
			
	listCategoryAdmin( $_REQUEST['msg'] );
			   
	require_once ( $Server.$SysRootInc."footer.php" ); 

?>
