
<?

error_reporting( E_ERROR );
ini_set( 'display_errors', '1' );

session_start();

// New Session Vars - Not in use
/*
$UserID 		= $_SESSION['EMOUser']['ID'];
$UserFirstName 	= $_SESSION['EMOUser']['FirstName'];
$UserLastName 	= $_SESSION['EMOUser']['LastName'];
$UserGroup 		= $_SESSION['EMOUser']['UserGroup'];
$UserEmail 		= $_SESSION['EMOUser']['Email'];	
*/


$ErrorMsg		= "";
$Title			= 'EMO Document Share';

$Server			= $_SERVER['DOCUMENT_ROOT'];

$Root			= "/EMOSecureApp/";
$SysRoot 		= $Root."System/";

$SysRootInc 	= $SysRoot."Includes/";
$SysRootStyles 	= $SysRoot."Styles/";
$SysRootScripts = $SysRoot."Scripts/";
$SysRootImages 	= $SysRoot."Images/";

require( $Server.$SysRootInc."Functions.php" ); 

// $_SERVER['DOCUMENT_ROOT']."/EMOSecureApp/System/Includes/"

?>