<?
	// Created by Gian Pompilio, HRM Web Services (September 24-09)
	// Contact pompilg@halifax.ca for information
 	
	// Load functions
	require_once( "init.php" );
	
	// Check user login details
	CheckLoginStatus(1);	// 1 = admins only

	if( isset( $_GET['a'] ) ) {	

		switch( $_GET['a'] ) {

			case "remove":
				removeUser( $_GET['id'], $_GET['c'] );
				header( 'Location: users.php?c='.$_GET['c'] );
				break;

			case "delete":
				deleteUser( $_GET['id'] );
				header( 'Location: users.php' );
				break;

			case "activate":
				activateUser( $_GET['id'] );
				$url = 'Location: users.php';
				if( isset( $_GET['id'] ) )	$url .='?u='.$_GET['id'];
				if( isset( $_GET['c'] ) )	$url .='&c='.$_GET['c'];
				if( isset( $_GET['id'] ) )	$url .='#'.$_GET['id'];
				header( $url );
				break;

			case "deactivate":
				deactivateUser( $_GET['id'] );
				$url = 'Location: users.php';
				if( isset( $_GET['id'] ) )	$url .='?u='.$_GET['id'];
				if( isset( $_GET['c'] ) )	$url .='&c='.$_GET['c'];
				if( isset( $_GET['id'] ) )	$url .='#'.$_GET['id'];
				header( $url );
				break;

			case "activateall":
				activateAllUsers();
				header( 'Location: users.php' );
				break;

			case "deactivateall":
				deactivateAllUsers();
				header( 'Location: users.php' );
				break;

		}
	
	}

	
	// Begin Assembling Page
 
	require_once( $Server.$SysRootInc."Header.php" );

//	echo '<pre>';
//	print_r( $_REQUEST );
//	echo '</pre>';

	listUsers( $_GET['c'] );
			
	require_once( $Server.$SysRootInc."footer.php" ); 
	
?>
