<?
			
		function dbConnect() {
		
			global $conn;
			$dbHost = "localhost";
			$dbUser = "EMO_secure";
			$dbPass = "3m053cur3";
			$dbName = "EMODocExchange";
			
			$conn = mssql_connect( $dbHost, $dbUser, $dbPass ) or die( 'Can not connect to server' );
			mssql_select_db( $dbName, $conn ) or die( 'Can not select database' );

		}
		
		
		// User Categories
		function UserCategories( $UID ) {

			global $conn;
			$Categories = array();
			$Count = 0;
		
			dbConnect();
	
			// SQL
			$sql = 'SELECT CID FROM CategoryAccess WHERE UID = "'.$UID.'" ORDER BY CID ASC';
			$rs = mssql_query( $sql, $conn ) or die( 'Query Error' );

			// Loop record set
			while( $row = mssql_fetch_array( $rs ) ) {
				
				$Categories[$Count] = $row["CID"];
				$Count++;

			}
	
			mssql_close( $conn );
			return $Categories;

		}	
		



		// List Menu Items
		function listMenuItems( $UID, $UserGroupID ) {
		
			global $conn;
			$Categories = array();
			$Count = 0;

			// if a user id is present load their category access into an array (must happen before db connection is re-opened
			if( isset( $UID ) ) {			

				$Categories = UserCategories( $UID );

			}
		
			dbConnect();
	
			// SQL
			$sql 	= "SELECT * FROM Categories ORDER BY CID ASC";
			$rs 	= mssql_query( $sql, $conn ) or die( 'Query Error' );

			// Loop record set
			while( $row = mssql_fetch_array( $rs ) ) {
			
				// check the boxes that the user has access to
				if( $Categories[$Count] == $row["CID"] || $UserGroupID < 3 ) {

					echo '<li><a href="Home.php?p='.$row["CID"].'">'.$row["Name"].'</a></li>';
					$Count++;
			
				}
			
			}
	
			mssql_close( $conn );

		}			
		
		
		
		
		// List Category Checkboxes
		function listCategoryCheckboxes( $UID ) {
		
			global $conn;
			$Categories = array();
			$Count = 0;

			// if a user id is present load their category access into an array (must happen before db connection is re-opened
			if( isset( $UID ) ) {			
				$Categories = UserCategories( $UID );

			}
			
			dbConnect();
	
			// SQL
			$sql = 'SELECT * FROM Categories';
			$rs = mssql_query( $sql, $conn ) or die( 'Query Error' );

			// Loop record set
			$i = 1;
			while( $row = mssql_fetch_array ( $rs ) ) {
			
				echo '<span style="padding-top:5px;padding-bottom:2px;display:block">';
				
				// check the boxes that the user has access to
				if( $Categories[$Count] == $row["CID"] ) {
				
					echo '<input class="CheckBox" type="checkbox" name="fCategory'.$i.'" id="fCategory'.$i.'" value="'.$row["CID"].'" checked />';				
					$Count++;
					
				} else {
				
					echo '<input class="CheckBox" type="checkbox" name="fCategory'.$i.'" id="fCategory'.$i.'" value="'.$row["CID"].'" />';
				}
				
				echo '<label for="fCategory'.$i.'">('.$row["CID"].') '.$row["Name"].'</label>'
					.'</span>';

				$i++;
				
			}
	
			mssql_close( $conn );

		}
		
		
		
		
		// List Categories (NOT IN USE)
		function listCategoryDropDownOptions( $CID ) {

			global $conn;

			$sql = "SELECT * FROM Categories";
	
			$output = '<form id="fCategoryList" name="fCategoryList" style="display:inline;float:right;" action="Users.php?x=1" method="post">'
					 .'<select id="fCID" name="fCID" onchange="if(options[selectedIndex].value) { window.location.href=\'users.php?c=\'+(options[selectedIndex].value) } else { window.location.href=\'users.php\' }">'
					 .'<option value=""';
			
			if( !isset( $CID ) ) {
				
				$output .= ' selected ';

			}

			$output .= '>-All-</option>';

			dbConnect();

			$rs = mssql_query( $sql, $conn ) or die( 'Query Error' );

			// Loop record set
			while( $row = mssql_fetch_array( $rs ) ) {
			
				if( $CID == $row["CID"] ) {
						
					$output .= '<option id="'.$row["Name"].'" name="'.$row["Name"].'" value="'.$row["CID"].'" selected>'.$row["Name"].'</option>';
				
				} else {

					$output .= '<option id="'.$row["Name"].'" name="'.$row["Name"].'" value="'.$row["CID"].'">'.$row["Name"].'</option>';
				
				}

			}
			
			mssql_close ( $conn );
		
			$output .= '</select>'
					//.'<input id="fSubmit" name="fSubmit" class="Button" type="Submit" value="Go" style="width:23px !important; padding:2px;" /> '
					.'</form>';
			
			return $output;
	
		}
		
		
		


		function listCategoryAdmin( $msg ) {

			global $conn;

			// SQL
			$sql = "SELECT * FROM Categories ORDER BY CID";

			$output = '<form id="CategoryUpdateForm" name="CategoryUpdateForm" method="post" action="categories.php" >'
					.'<table class="Categories">'
					.'<tr><td class="Label MenuDivider" colspan="3">Edit Categories';

			if( isset( $msg ) ) {
				$output .= '<br /><span style="color:red;font-weight:normal;">'.$msg.'</span>';
			}

			$output .= '</td></tr>';

			dbConnect();
			
			$rs = mssql_query( $sql, $conn ) or die( 'Query Error' );

			// Loop record set
			while( $row = mssql_fetch_array( $rs ) ) {	
			
				$output .= '<tr><td>['.$row["CID"].'] </td><td><input name="f'.$row["CID"].'" type="text" id="f'.$row["CID"].'" value="'.$row["Name"].'" /></td>';
				$output .= '<td style="width:50px;"><a title="Archive: '.$row["Name"].'" href="Categories.php?fState=Archive&cid='.$row["CID"].'&title='.str_replace(" ","-",$row["Name"]).'" onclick="javascript:ConfirmArchive(this, \''.str_replace(" ","-",$row["Name"]).'\',\''.$row["CID"].'\');"><img src="System/Images/database_save.png" /></a>';
				//$output .= '&nbsp&nbsp&nbsp<a title="Click to Delete: '.$row["Name"].'" href="Categories.php?fState=Delete&cid='.$row["CID"].'" onclick="javascript:ConfirmDeleteCategory(this, \''.$row["Name"].'\');"><img src="System/Images/delete.png" /></a>';
				$output .= '</td></tr>';
				$nextID = $row["CID"];
			
			}
	
			mssql_close( $conn );
			
					
			$output .= '<tr><td colspan="4"><p>Improve how categories appear in the left hand menu by naming them as concise as possible.</p><p>Names clearly describe the event (ie. "Hurrican Danny") but board enough to be easily recycled for similar event (ie. "Hurrican Danny" could easily become "Hurrican Joe").</p></td><td></td></tr>'
					.'<tr><td colspan="4"><input type="hidden" id="fState" name="fState" value="Update" /><input class="Button" type="submit" value="Update" /></td></tr></table></form>';
					
			$output .= '<br /><br />'
					.'<form id="CategoryAddForm" name="CategoryAddForm" method="post" action="categories.php" >'
					.'<table class="UserListing">'
					.'<tr><td class="Label MenuDivider" colspan="3">Add New Category</td></tr>'
					.'<tr><td>Name</td><td><input class="Categories" name="fCategoryName" type="text" id="fCategoryName" value="" /></td><td style="width:15px;"><img src="System/Images/add.png" /></td></tr>'
					//.'<tr><td class="Title">URL</td><td><input class="Categories" name="fCategoryURL" type="text" id="fCategoryURL" value="" /></td></tr>'
					.'<tr><td colspan="3"><p>A new category file will be created and added to the Categories folder automatically. The new filename will be displayed in the left hand column (above) once it has been added.</p></td></tr>'
					.'<tr><td colspan="3"><input type="hidden" id="fState" name="fState" value="Add" /><input class="Button" type="submit" value="Add" name="submitform" /></td></tr></table></form>';
					
			echo $output;

		}	








		// List Users
		function listUsers( $CID ) {

			$count = 0;
			$emailAddresses = "";
			$categoryDropDownMenu = listCategoryDropDownOptions( $CID );

			global $conn;
			dbConnect();

			$sql = 'SELECT * FROM Users ORDER BY FirstName ASC';			

			if( $CID ) {
			
				$sql = 'SELECT * FROM UserCategoryAccess WHERE CID = '.$CID.' ORDER BY FirstName ASC';			
			}

			$rs = mssql_query( $sql, $conn ) or die( 'Query Error' );

			// Loop record set				
			while( $row = mssql_fetch_array( $rs ) ) {	
				
				
				// Add back to top every x users
				if( ($count % 10) == 9 ) {
				$output .= '<tr><td class="User" style="padding-bottom:5px;">'
						.'<span style="float:right;"><a href="#top">Back to Top</a></span>'
						.'</td></tr>';
				}
				
				$output .= '<tr>'
						.'<td';
						
				if( $_GET['u'] == $row["UID"] ) {	
					$output .= ' class="UserActive" ';
				} else {
					$output .= ' class="User" ';
				}

				$output .='title="'.$row["FirstName"].' '.$row["LastName"].'">'
						.'<span style="float:right;">';

				$output .='<a href="mailto:'.$row["Email"].'?subject=EMO Document Share" title="Send '.$row["FirstName"].' '.$row["LastName"].' an email message."><img src="System/Images/email.png" style="height:14px; padding-right:10px;" /></a>';

				// Show delete or remove icon depending on the type of list
				if(isset($CID))
					$output .= '<a href="users.php?a=remove&id='.$row["UID"].'&c='.$CID.'" onclick="javascript:ConfirmRemove(this,\''.$row["FirstName"].' '.$row["LastName"].'\');" title="Remove '.$row["FirstName"].' '.$row["LastName"].' from this category?"><img src="System/Images/remove.png" style="height:14px;" /></a>';
				else 
					$output .= '<a href="users.php?a=delete&id='.$row["UID"].'" onclick="javascript:ConfirmDelete(this,\''.$row["FirstName"].' '.$row["LastName"].'\');" title="Delete '.$row["FirstName"].' '.$row["LastName"].'?"><img src="System/Images/delete.png" style="height:14px;" /></a>';
					
				$output .= '</span>';



				if( $row["Active"] == 1 ) {

					$output .='<span style="float:left;"><a href="users.php?a=deactivate&id='.$row["UID"].'&c='.$CID.'" title="'.$row["FirstName"].' '.$row["LastName"].' is active - click to deactivate." onclick="javascript:ConfirmDeactivate(this,\''.$row["FirstName"].' '.$row["LastName"].'\');"><img src="System/Images/bullet_green.png" style="height:14px; padding-right:6px;" /></a></span> ';

				} elseif( $row["Active"] == 0 ) {
					
					$output .='<span style="float:left;"><a href="users.php?a=activate&id='.$row["UID"].'&c='.$CID.'" title="'.$row["FirstName"].' '.$row["LastName"].' is inactive - click to activate." onclick="javascript:ConfirmActivate(this,\''.$row["FirstName"].' '.$row["LastName"].'\');"><img src="System/Images/bullet_red.png" style="height:14px; padding-right:6px;" /></a></span> ';
				
				}

				$output .= '<span style="float:left;"><a name="'.$row["UID"].'"></a><a href="user.php?id='.$row["UID"].'" title="Click to modify '.$row["FirstName"].' '.$row["LastName"].'\'s profile.">'.$row["FirstName"].' '.$row["LastName"].'</a></span> '
						.'</td>'
						.'</tr>';
					
				$count++;
				
				if( $emailAddresses == "" ) {
					$emailAddresses = $row["Email"];

				} else {
					$emailAddresses .= ','.$row["Email"];
					
				}
				
			}
			
			mssql_close( $conn );

			if( $count == 0 ) {
				$output = '<tr><td style="padding-top:20px;padding-bottom:20px;">No users to display.</td></tr>';
			}
					
			$output .= '<tr><td>'
					.'<span style="float:right;"><a href="#top">Back to Top</a></span>'
					.'</td></tr>'
					.'</table>';
		
			// Create header
			$header = '<table class="UserListing">'
					.'<tr><td class="Label MenuDivider">' 
					.$categoryDropDownMenu
					.$count.' Users</td></tr>'
					.'<tr><td class="DocLinks" style="text-align:center;padding-bottom:5px;">'
					.'<span><a href="mailto:'.$emailAddresses.'?subject=EMO Document Share">Email all</a>';
			
			if( !isset( $CID ) ) {
				$header .= '&nbsp&nbsp�&nbsp&nbsp<a href="users.php?a=activateall" onclick="javascript:ConfirmActivateAll(this);">Activate all</a>&nbsp&nbsp�&nbsp&nbsp<a href="users.php?a=deactivateall" onclick="javascript:ConfirmDeactivateAll(this);">Deactivate all</a></span>';
			}
			
			$header .= '</td></tr><td><tr></td></tr>';
					
			echo $output = $header.$output;
	
		}	




		// Get Page Title 		
		function pageTitle( $x ) {

			$output = 'Title Not Defined';

			global $conn;		
			dbConnect();
	
			$sql = 'SELECT * FROM Categories WHERE CID = "'.$x.'"';
			$rs = mssql_query( $sql, $conn ) or die( 'Query Error' );
			
			if( $row = mssql_fetch_array( $rs ) ) {
				$output = $row["Name"];
			}
			
			mssql_close( $conn );
			
			return $output;

		}	
		


		// Get User Details	
		function getUserDetails( $id ) {
		
			global $conn;
			dbConnect();
	
			// SQL
			$sql = 'SELECT * FROM Users WHERE UID = "'.$id.'"';
			$rs = mssql_query( $sql, $conn ) or die( 'Query Error' );

			// Loop record set
			if( $row = mssql_fetch_array( $rs ) ) {
			
				$output = array( $row['UID'], $row['FirstName'], $row['LastName'], $row['Email'], $row['Password'], $row['UserGroup'], $row['Active'] );
				return $output;
				
			}
	
			mssql_close( $conn );

		}	



		
		
		// userIsAdmin 
		function userIsAdmin( $UID ) {
		
			global $conn;
			dbConnect();
	
			// SQL
			$sql = 'SELECT UserGroup FROM Users WHERE UID = "'.$UID.'"';
			$rs = mssql_query( $sql, $conn ) or die( 'Query Error' );

			// Loop record set
			while( $row = mssql_fetch_array( $rs ) ) {
			
				if( $row["UserGroup"] < 3 ) {
					//echo "Checked";
					return true;
				}
			
			}
	
			mssql_close( $conn );

		}		


		
		
		// userIsSuperAdmin 
		function userIsSuperAdmin( $UID ) {
		
			global $conn;
			dbConnect();
	
			// SQL
			$sql = 'SELECT UserGroup FROM Users WHERE UID = "'.$UID.'"';
			$rs = mssql_query( $sql, $conn ) or die( 'Query Error' );

			// Loop record set
			while( $row = mssql_fetch_array( $rs ) ) {
			
				if( $row["UserGroup"] == 1 ) {
					return true;
				}
			
			}
	
			mssql_close( $conn );

		}	


	
		



		
		
		// Insert or Update user	
		function UserInsertUpdate( $data ) {

			$UserGroup = 3; 	// default usergroup (user)
			$Active = 1;		// default user active
			global $conn;

			// Set inactive if necessary
			if( !isset( $data['fActive'] ) )
				$Active = 0;

/*			// Set usergroup to admin if necessary
			if( isset( $data['fIsAdmin'] ) )	
				$UserGroup = $data['fIsAdmin'];
		
			if( $data['fIsSuperAdmin'] == "true" ) {

				header( 'Location: user.php?msg=1&id='.$data['fID'] );

			} else {*/
				
				dbConnect();
			
				// Process updates
				if( $data['fState'] == 'update' ) {

					// Update user
					$sql = 'UPDATE Users SET '
						 .'FirstName = "'.$data['fFirstName'].'", '
						 .'LastName = "'.$data['fLastName'].'", '
						 .'Email = "'.$data['fEmail'].'", '
						 .'Password = "'.$data['fPassword'].'", '
						 //.'UserGroup = "'.$UserGroup.'", '
						 .'UserGroup = "'.$data['fUserType'].'", '
						 .'Active = "'.$Active.'" '
						 .'WHERE UID = '.$data['fID'];

					$rs = mssql_query( $sql, $conn ) or die( 'Error updating user information.' );
				
					// Remove users category access
					$sql = 'DELETE FROM CategoryAccess WHERE UID = "'.$data["fID"].'"';
					$rs = mssql_query( $sql, $conn ) or die( 'Error deleting user category access.' );
				
					// Assign new category access rights
					foreach($data as $key => $value) {
						if( substr( $key, 0, 9 ) == 'fCategory' ) {			
							$sql = 'INSERT INTO CategoryAccess (UID, CID) VALUES ("'.$data["fID"].'", "'.$value.'")';
							$rs = mssql_query( $sql, $conn ) or die( 'Error updating user category access information.' );
						}
					}
					
					mssql_close( $conn );
					
					// Redirect to user list and focus on the recently modified user
					header( 'Location: users.php?u='.$data["fID"].'#'.$data["fID"] );
					
									
				
				// Process New Users (inserts)
				} elseif ( $data['fState'] == 'insert' ) {

					//Check if users email is already in the system
					echo $sql = 'SELECT * FROM Users WHERE Email = "'.$data['fEmail'].'"';
					$rs = mssql_query( $sql, $conn ) or die( 'Error checking for user.' );

					if( $row = mssql_fetch_array( $rs ) ) {
						
						header( 'Location: user.php?msg=2&id='.$row['UID'] );	

					} else {

						$sql = 'INSERT INTO Users (FirstName, LastName, Email, Password, UserGroup, Active) VALUES ("'
							 .$data['fFirstName'].'", "'
							 .$data['fLastName'].'", "'
							 .$data['fEmail'].'", "'
							 .$data['fPassword'].'", '
							 .$data['fUserType'].', '
							 //.$UserGroup.', '
							 .$Active
							 .')';
							 
						echo $sql;

						$rs = mssql_query( $sql, $conn ) or die( 'Error inserting new user.' );	
						
						// Confirm that the new user was added to the dbase
						$sql = 'SELECT * FROM Users WHERE FirstName = "'.$data['fFirstName'].'" AND LastName = "'.$data['fLastName'].'" AND Email = "'.$data['fEmail'].'"';
						$rs = mssql_query( $sql, $conn ) or die( 'Error selecting newly added user.' );			
						$row = mssql_fetch_array( $rs ) or die( 'Error fetching newly added user. ' );
						$newUserId = $row['UID'];
						
						// Assign new category access rights
						foreach($data as $key => $value) {
							if( substr( $key, 0, 9 ) == 'fCategory' ) {			
								$sql2 = 'INSERT INTO CategoryAccess (UID, CID) VALUES ("'.$newUserId.'", "'.$value.'")';
								$rs2 = mssql_query( $sql2, $conn ) or die( 'Error assigning new users category access information.' );
							}
						}
												
						// Redirect to user list and focus on the recently modified user
						//header( 'Location: users.php?u='.$newUserId.'#'.$newUserId );
						//header("Location: http://hrmwin53/_private/EMODocExchangeSendmailNEW.php?n=".$row['FirstName']."&u=".$row['Email']."&p=".$row['Password']."&te=".$row['Email']."&t=1"."&id=".$newUserId);
						
						// Temporary Fix.
						//header("Location: http://www.gianpompilio.com/EMOSecureApp/SendMail.php?n=".$row['FirstName']."&u=".$row['Email']."&p=".$row['Password']."&te=".$row['Email']."&t=1"."&id=".$newUserId);
						header("Location: http://eservices.halifax.ca/_private/EMODocExchangeSendmailNEW.php?n=".$row['FirstName']."&u=".$row['Email']."&p=".$row['Password']."&te=".$row['Email']."&t=1"."&id=".$newUserId);
						
						mssql_close( $conn );


					}

				}
	
			/*}*/			

		}	
		
		
		
		
		// Delete User... 		
		function deleteUser( $UID ) {
		
			global $conn;
			dbConnect();
	
			// Remove User
			$sql = 'DELETE FROM Users WHERE UID ="'.$UID.'"';
			$rs = mssql_query( $sql, $conn ) or die( 'Error removing user.' );

			// Remove users category access
			$sql = 'DELETE FROM CategoryAccess WHERE UID = "'.$UID.'"';
			$rs = mssql_query( $sql, $conn ) or die( 'Error removing user category access.' );

			mssql_close( $conn );

		}	
		



		// Activate User... 		
		function removeUser( $UID, $CID ) {
		
			global $conn;
			dbConnect();
	
			$sql = 'DELETE FROM CategoryAccess WHERE UID = "'.$UID.'" AND CID = "'.$CID.'"';
			$rs = mssql_query( $sql, $conn ) or die( 'Error remove user from this category.' );

			mssql_close( $conn );

		}

		// Activate User... 		
		function resetUser( $UID ) {
		
			global $conn;
			dbConnect();
	
			$sql = 'DELETE FROM CategoryAccess WHERE UID = "'.$UID.'"';
			$rs = mssql_query( $sql, $conn ) or die( 'Error reseting users category access.' );

			mssql_close( $conn );

		}

		// Activate User... 		
		function activateUser( $UID ) {
		
			global $conn;
			dbConnect();
	
			$sql = 'UPDATE Users SET '
				 .'Active = 1 '
				 .'WHERE UID = '.$UID;

			$rs = mssql_query( $sql, $conn ) or die( 'Error updating user.' );
	
			mssql_close( $conn );

		}



		// Deactivate User... 		
		function deactivateUser( $UID ) {
		
			global $conn;
			dbConnect();
	
			$sql = 'UPDATE Users SET '
				 .'Active = 0 '
				 .'WHERE UID = '.$UID;

			$rs = mssql_query( $sql, $conn ) or die( 'Error updating user.' );
	
			mssql_close( $conn );

		}




		// Activate All User... 		
		function activateAllUsers() {
		
			global $conn;
			dbConnect();
	
			$sql = 'UPDATE Users SET Active = 1';
			$rs = mssql_query( $sql, $conn ) or die( 'Error activating users.' );

			mssql_close( $conn );

		}



		// Deactivate All User... 		
		function deactivateAllUsers() {
		
			global $conn;
			dbConnect();
	
			$sql = 'UPDATE Users SET Active = 0';
			$rs = mssql_query( $sql, $conn ) or die( 'Error deactivating users.' );
	
			mssql_close( $conn );

		}
		


		
		// convert category id to name	
		function getUserGroupName( $id ) {
		
			global $conn;
			dbConnect();
	
			// SQL
			$sql = 'SELECT * FROM UserGroups WHERE GID = '.$id;
			$rs = mssql_query( $sql, $conn ) or die( 'Query Error' );

			// Loop record set
			if( $row = mssql_fetch_array( $rs ) ) {
				$output = $row['Name'];
			}
	
			mssql_close( $conn );
			
			return $output;

		}		

		



		
			


function CheckVars( $x ) {
	
	global $ErrorMsg;
	global $Title;
	
	if( isset( $x ) ) {	
	
		$ErrorMsg = "<tr><td class='ErrorMsg' colspan='2'>";
	
		switch( $x ) {
		
			case '1':
				$ErrorMsg .= 'You must be logged in to participate.'; 
				break;
				
			case '2':
				$ErrorMsg .= 'Invalid login credentials.';
				break;
	
			case '3':
				$ErrorMsg .= 'Invalid password.';
				break;
				
			case '4':
				$ErrorMsg .= 'You have been logged out.';
				break;
				
			case '5':
				$ErrorMsg .= 'Credentials have been sent to your email.';
				break;
				
			case '6':
				$ErrorMsg .= 'That email does not exist in our system.<br />If you have questions or concerns, please contact <a href="mailto:HRM_EMO@Halifax.ca?subject=EMO Account Inactive">HRM_EMO@Halifax.ca</a>.';
				break;	
				
			case '7':
				$ErrorMsg .= 'Your account is currently inactive.<br />Please contact the administrator (<a href="mailto:HRM_EMO@Halifax.ca?subject=EMO Account Inactive">HRM_EMO@Halifax.ca</a>) if you have any questions.';
				break;								
	
		}
		
		$ErrorMsg .= "</td></tr>";
		return $ErrorMsg;
	
	}

}
		

		// Login 		
		function Login( $Email, $Password ) {

			if( empty( $_SESSION['EMOUser']['ID'] ) ) { // Ensure no one is logged in

				if( isset( $Email ) && isset( $Password ) ) { // Look for Email and Password
					global $conn;
					dbConnect();

					// Prevent SQL injection by striping email of illegal characters
					$IllegalCharacters = array( "'", "=", ";", ":" );
					$Email = str_replace( $IllegalCharacters, "", $Email );
				
					// Further prevent SQL injection by limiting the size of Email
					// $Email = substr($Email, 0, 8);

					// SQL
					$sql = 'SELECT * FROM Users WHERE Email = "'.$Email.'"';
					$rs = mssql_query( $sql, $conn ) or die( 'Email not found in database.' );
		
					// Loop record set
					if( $row = mssql_fetch_array( $rs ) ) {
					
						// If the password doesn't match, redirect to login page with error
						if( $row['Password'] != $Password ) {
							header( 'Location: index.php?x=3' );
						}
						
						// If user is inactive, redirect to login page with error
						elseif( $row['Active'] == 0 ) {
							header( 'Location: index.php?x=7' );
						}
						
						// If user does exist and autheticates correctly, create a session
						$_SESSION['EMOUser']['ID'] 			= $row['UID'];
						$_SESSION['EMOUser']['FirstName'] 	= $row['FirstName'];
						$_SESSION['EMOUser']['LastName'] 	= $row['LastName'];
						$_SESSION['EMOUser']['UserGroup'] 	= getUserGroupName( $row['UserGroup'] );
						$_SESSION['EMOUser']['UserGroupID'] = $row['UserGroup'];
						$_SESSION['EMOUser']['Email'] 		= $row['Email'];
						$_SESSION['EMOUser']['Active'] 		= $row['Active'];		
					
					// If that user does not exist, redirect to login page with appropriate error
					} else {
						header( 'Location: index.php?x=2' );
					}
		
					mssql_close( $conn );
					
				}
				
			} else {
				echo "user still logged in";
			}

		}	

	
		function CheckLoginStatus( $UserGroupID = 3 ) {
		
			if( empty( $_SESSION['EMOUser'] ) ) {
				header( 'Location: index.php?x=1' );

			} elseif( $_SESSION['EMOUser']['UserGroupID'] > $UserGroupID ) {
				header( 'Location: home.php' );
			}
						
		}
	



		function ShowPage( $UserID, $UserGroup, $Page ) {
		
			$Count = 0;
			
			// if a user id is present load their category access into an array (must happen before db connection is re-opened
			if( isset( $UserID ) ) {			
				$Categories = UserCategories( $UserID );
			}
			
			if( $UserGroup < 3 ) { // If user is admin or OpsO grant them access.

				if( isset( $Page ) ) {
					include( $_SERVER['DOCUMENT_ROOT']."/EMOSecureApp/Categories/".$Page."/index.php" );
				
				} else {
					include( $_SERVER['DOCUMENT_ROOT']."/EMOSecureApp/Categories/Intro.php" );	
				}

			} else { // otherwise, check that they have permsission to access that page.

				for( $i=0; $i<count($Categories); $i++ ) {
				
					if( $Page == $Categories[$i]) {
						include( $_SERVER['DOCUMENT_ROOT']."/EMOSecureApp/Categories/".$Page."/index.php" );
						$Count++;
						break;
					}
				
				}
				
				if( $Count == 0 && isset( $Page ) ) {
					include( $_SERVER['DOCUMENT_ROOT']."/EMOSecureApp/Categories/PermissionDenied.html" );	
					
				} elseif ( $Count == 0 ) {
					include( $_SERVER['DOCUMENT_ROOT']."/EMOSecureApp/Categories/Intro.php" );	
				}

			}
					
		}






function userForgetPassword($email)
{
	global $db;
	ConnectDB();

	$sqlQuery = "SELECT * "
              . "FROM AppUser "
              . "WHERE (Email1 = '" . $email . "') OR (Email2 = '" . $email . "')";
	$rs = $db->Execute($sqlQuery);
	$row = $rs->FetchRow(); 
	
	//If Rows are Returned
	if($rs->RowCount())
	{
		$username 	= $row['Username'];
		$fname 		= $row['FirstName'];
		$lname 		= $row['LastName'];
		$active 	= $row['IsActive'];
		$password 	= base64_encode($row['Password']);
		
		//Pass the Users Information including the encrypted password to the form script located on the EServices Server.
		header("Location: http://eservices.halifax.ca/_private/hrpcalendar/form.php?e=$email&u=$username&fn=$fname&ln=$lname&p=$password&a=$active");
	}
	else //No Rows Returned
	{	
		//If No Rows are Returned the User entered invaild Information so kick them back to the Forgotten Password page
		header("Location: http://www.halifax.ca/Police/VolunteerCalendar/login.php?action=FORGOT&status=FAIL");	
	}
}





function RemoveDirectory($dir, $virtual = false)
{
	$ds = DIRECTORY_SEPARATOR;
	$dir = $virtual ? realpath($dir) : $dir;
	$dir = substr($dir, -1) == $ds ? substr($dir, 0, -1) : $dir;
	if (is_dir($dir) && $handle = opendir($dir))
	{
		while ($file = readdir($handle))
		{
			if ($file == '.' || $file == '..')
			{
				continue;
			}
			elseif (is_dir($dir.$ds.$file))
			{
				RemoveDirectory($dir.$ds.$file);
			}
			else
			{
				unlink($dir.$ds.$file);
			}
		}
		closedir($handle);
		rmdir($dir);
		return true;
	}
	else
	{
		return false;
	}
}

function smartCopy($source, $dest, $folderPermission=0755,$filePermission=0644){
# source=file & dest=dir => copy file from source-dir to dest-dir
# source=file & dest=file / not there yet => copy file from source-dir to dest and overwrite a file there, if present

# source=dir & dest=dir => copy all content from source to dir
# source=dir & dest not there yet => copy all content from source to a, yet to be created, dest-dir
    $result=false;
   
    if (is_file($source)) { # $source is file
        if(is_dir($dest)) { # $dest is folder
            if ($dest[strlen($dest)-1]!='/') # add '/' if necessary
                $__dest=$dest."/";
            $__dest .= basename($source);
            }
        else { # $dest is (new) filename
            $__dest=$dest;
            }
        $result=copy($source, $__dest);
        chmod($__dest,$filePermission);
        }
    elseif(is_dir($source)) { # $source is dir
        if(!is_dir($dest)) { # dest-dir not there yet, create it
            @mkdir($dest,$folderPermission);
            chmod($dest,$folderPermission);
            }
        if ($source[strlen($source)-1]!='/') # add '/' if necessary
            $source=$source."/";
        if ($dest[strlen($dest)-1]!='/') # add '/' if necessary
            $dest=$dest."/";

        # find all elements in $source
        $result = true; # in case this dir is empty it would otherwise return false
        $dirHandle=opendir($source);
        while($file=readdir($dirHandle)) { # note that $file can also be a folder
            if($file!="." && $file!="..") { # filter starting elements and pass the rest to this function again
#                echo "$source$file ||| $dest$file<br />\n";
                $result=smartCopy($source.$file, $dest.$file, $folderPermission, $filePermission);
                }
            }
        closedir($dirHandle);
        }
    else {
        $result=false;
        }
    return $result;
    } 




?>