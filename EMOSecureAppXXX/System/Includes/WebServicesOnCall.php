
<table style="width:100%;padding-top:12px;">

<tr>
	<td colspan="2"><h3>Web Services On Call Schedule</h3></td>
</tr>

<tr>
	<td colspan="2"><p>The Web Services team will be activated by the Manager of Web Services prior to an EMO event.</p><br /></td>
</tr>

<tr>
	<td class="Label"><p>Staff Member</p></td>
	<td class="Label"><p>Shift Schedule</p></td>
</tr>

<tr>
	<td><p><a href="mailto:pompilg@halifax.ca?Subject='EMO'">Gian Pompilio</a></p></td>
	<td>TBA</td>
</tr>

<tr>
	<td><p><a href="mailto:marneyk@halifax.ca?Subject='EMO'">Kelly Marney</a></p></td>
	<td>TBA</td>
</tr>

<tr>
	<td><p><a href="mailto:udbyb@halifax.ca?Subject='EMO'">Beth Udby</a></p></td>
	<td>TBA</td>
</tr>

<tr>
	<td><p><a href="mailto:simpsos@halifax.ca?Subject='EMO'">Shaun Simpson</a></p></td>
	<td>TBA</td>
</tr>


</table>