<?php
	// Capture parameters sent over secure connection
	$UserInfo = $_GET;
	$EOL = "\n";
	$EOL2 = "\n\n";
	
	if( $_GET ) {
		
		$type = $UserInfo['t'];
		
		// Determine type of email to send out
		// 1 = New User Welcome
		if( $type ==1 ) {
			$EmailTypeMsg = "Welcome to the EMO Data Share.";
	
		// 2 = Forgot Password
		} else if( $type == 2 ) {
			$EmailTypeMsg = "We received a 'forgot password' request.";
	
		// Default / Error
		} else {
			exit("Could not determine the 'type' of email to dispatch.");
		}
		
		// Assemble output
		$Result = "Hi ".$UserInfo['n'].",".$EOL2
				 .$EmailTypeMsg.$EOL2
				 ."Your login credentials are included below. You can login to the EMO Data Share by visiting:".$EOL
				 ."https://www.halifax.ca/EMOSecureApp/".$EOL2
				 ."Username: ".$UserInfo['u'].$EOL
				 ."Password: ".$UserInfo['p'].$EOL2
				 ."If you required technical assistance, please email webservices@halifax.ca".$EOL
				 ."For EMO inquiries, please email hrm_emo@halifax.ca";
	
		// Test Data
		//echo "To Email: ".$UserInfo['te'].$EOL;
		//echo "From Email: ".$UserInfo['fe'].$EOL;
		//echo "Subject: EMO Data Share - Login credentials".$EOL2;
		//echo $Result.$EOL;
		
		$FromEmail = "webservices@gmail.com";
			
		$to      = $UserInfo['te'];
		$subject = 'EMO Data Share - Login credentials';
		$message = $Result;
		$headerData = "From: $FromEmail";
		
		if( mail( $to, $subject, $message, $headerData ) ) {

			if( $type == 1 ) {
					header('Location: https://www.halifax.ca/EMOSecureApp/users.php?u='.$UserInfo['id'].'#'.$UserInfo['id'] );
			} 
			
			if( $type == 2 ) {
					header('Location: https://www.halifax.ca/EMOSecureApp/index.php?x=5');
			}
			
		} else
			echo "Error dispatching email - Please report this issue to webservices@halifax.ca.";
		
	} else {
		echo "Error - No variables passed - Please report this issue to webservices@halifax.ca.";
		
	}
?>