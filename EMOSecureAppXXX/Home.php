<?
	// Created by Gian Pompilio, HRM Web Services (September 24-09)
	// Contact pompilg@halifax.ca for information
 	
	// Load functions
	require_once( "init.php" );
	


	if( isset( $_REQUEST['fEmail'] ) && isset ( $_REQUEST['fPassword'] ) ) {
		Login( $_REQUEST['fEmail'], $_REQUEST['fPassword'] );
	} else {
		CheckLoginStatus();
	}
	


	// Begin Assembling Page
	
	require_once( $Server.$SysRootInc."Header.php" );

	ShowPage( $_SESSION['EMOUser']['ID'], $_SESSION['EMOUser']['UserGroupID'], $_REQUEST['p'] );
			   
	require_once( $Server.$SysRootInc."footer.php" ); 

?>
