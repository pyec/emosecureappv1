<?
	// Created by Gian Pompilio, HRM Web Services (September 24-09)
	// Contact pompilg@halifax.ca for information
 	
	// Load functions
	require_once( "init.php" );
	session_destroy();

	// Process submitted information
	if( isset( $_POST['fUserEmail'] ) ) {

		global $conn;
		dbConnect();

		// SQL
		$sql = 'SELECT * FROM Users WHERE Email = "'.$_POST['fUserEmail'].'"';
		$rs = mssql_query( $sql, $conn );

		if( $row = mssql_fetch_array( $rs ) ) {
			// header("Location: http://hrmwin53/_private/EMODocExchangeSendmailNEW.php?n=".$row['FirstName']."&u=".$row['Email']."&p=".$row['Password']."&te=".$row['Email']."&t=2");
			// Temporary SendMail fix.
			//header("Location: http://www.gianpompilio.com/EMODocExchangeSendmailNEW.php?n=".$row['FirstName']."&u=".$row['Email']."&p=".$row['Password']."&te=".$row['Email']."&t=2");
			header("Location: http://eservices.halifax.ca/_private/EMODocExchangeSendmailNEW.php?n=".$row['FirstName']."&u=".$row['Email']."&p=".$row['Password']."&te=".$row['Email']."&t=2");

		} else {
			header('Location: http://www.halifax.ca'.$Root.'forgot.php?x=6');
		}

		mssql_close( $conn );

	}

	CheckVars( $_REQUEST['x'] );
	

 	require_once ( $Server.$SysRootInc."HeaderMetaEtc.php" ); 

?>

		<div id="Login" class="GStyle LessPadding">
	        
        	<div style="padding:25px !important;">     
             
                <h1><? echo $Title; ?></h1>
                
                <form action="forgot.php" method="post" name="fEMOSecure" id="EMOSecureLogin" onSubmit="return ForgotValidation(this);">
                    <table style="padding-top:5px;">
                    	<? echo $ErrorMsg; ?>
                        <tr>
                            <td colspan="2" class="Description">To retrieve your login credentials, enter your email address below.</td>
                        </tr>
                        <tr>
                            <td class="Label"><label for="fEmail">Email: </label></td>
                            <td><input id="fUserEmail" name="fUserEmail" type="text" maxlength="50" /></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="padding-top:5px;"><input id="fSubmit" name="fSubmit" class="Button" type="Submit" value="Send" /><a href="index.php">Return to login</a></td>
                        </tr>                       
                    </table>
                </form>
                
<?	require_once ( $Server.$SysRootInc."footer.php" ); ?>