<?

	// Created by Gian Pompilio, HRM Web Services (September 24-09)
	// Contact pompilg@halifax.ca for information
 	
	// Load functions
	require_once( "init.php" );
	
	session_destroy();
	CheckVars ( $_REQUEST['x'] ) ;

 	require_once ( $Server.$SysRootInc."HeaderMetaEtc.php" ); 

?>

		<div id="Login" class="GStyle LessPadding">
	        
        	<div style="padding:25px !important;">       
             
                <h1><? echo $Title; ?></h1>
                
                <form action="Home.php" method="post" name="fEMOSecure" id="EMOSecureLogin" onSubmit="return LoginValidation(this);">
                               
  <table style="padding-top:5px;">              
	<? echo $ErrorMsg; ?>
    <tr>
      <td colspan="2" class="Description">Enter your login credentials below.</td>
    </tr>
    <tr>
      <td class="Label"><label for="fEmail">Email: </label></td>
      <td><input id="fEmail" name="fEmail" type="text" maxlength="75" /></td>
    </tr>
    <tr>
      <td class="Label"><label for="fPassword">Password: </label></td>
      <td><input id="fPassword" name="fPassword" type="password" maxlength="10" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td style="padding-top:5px;"><input id="fSubmit" name="fSubmit" class="Button" type="Submit" value="Login" /> <a href="<? echo $Root; ?>forgot.php">Forgot login?</a></td>
    </tr>
    </table>
  <a href="https://www.halifax.ca/emosecureapp/documents/140107FedSEMPOutline.doc"><a></a></a>
</form>
                
<?	require_once ( $Server.$SysRootInc."footer.php" ); ?>
