
<?php
  /*---------------------------------------------------------------
   * 
   * 	MODULE:		home.php
   * 	AUTHOR:		Gian
   * 	Created:	Unknown
   * 
   * --------------------------------------------------------------
   * 
   * 	
   * 	MODIFICATION HISTORY
   * 	20170519	PRSC	Cleanup and PHP tabs fixed.
   *    * 
   *---------------------------------------------------------------
   */

  include ( $Server.$SysRootInc."HeaderMetaEtc.php" );

  ?>

<a name="top"></a>

<span style="float:right;padding-top:6px;padding-right:7px;">
	
  <?php

	  echo $_SESSION['EMOUser']['FirstName'].' '. 
	  			$_SESSION['EMOUser']['LastName'].' ('.$_SESSION['EMOUser']['UserGroup'].')'; 
	  			
	 ?>
	  � 
    <a href="index.php?x=4">Logout</a>
    <!-- � <a href="mailto:EMOSecure@halifax.ca?subject=EMO Document Share Question">Email Admin</a>-->
</span>

<h2>
	<a href="home.php" title="Return to Homepage">
	<?php echo $Title; ?></a></h2>

<div class="ClearFloats"></div>

<?php 	include ( $Server.$SysRootInc."Menu.php" ); ?>

<div id="Content" class="GStyle LessPadding">

	<div>