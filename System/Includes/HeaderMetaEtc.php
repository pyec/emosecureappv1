<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	
	<head>

		<title>
		<?php echo $Title; ?>
		</title>
		
		<link rel="stylesheet" href="<?php echo $SysRootStyles . "Default.css"; ?>"
					 type="text/css" media="all" />
		<?php
		 include ($SysRootStyles . "IEExceptions.php"); 
		 ?>
		 
        <script type="text/javascript" 
        src="<?php echo $SysRootScripts . "Default.js"; ?>">
        </script>
        
	</head>

	<body>