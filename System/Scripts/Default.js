

//form validation
function LoginValidation(theForm) {
	
	var output = "";
	var highlightColor ="#ffe9e6";
	
	// reset form elements to white
	for (i=0; i<theForm.elements.length; i++)
		if (theForm.elements[i].type=="text" || theForm.elements[i].type=="textarea" || theForm.elements[i].type=="select-one")
			theForm.elements[i].style.background='#fff';
		
	// begin validating form elements
	if (theForm.fUsername.value == "") {
		output += "- Username\n";
		theForm.fUsername.style.background=highlightColor;
	}
/*
	} else if (ValidateEmail(theForm.fUsername)==false) { 
		output += "- Not a valid e-mail address!\n";
		theForm.fUsername.style.background=highlightColor;
	}
*/
	if (theForm.fPassword.value == "") {
		output += "- Password\n";
		theForm.fPassword.style.background=highlightColor;
	}

	// Format output (pass or fail validation)
	if (output != "") {
		output = "\nOops! You missed the following required field(s):\n\n" + output;
		//output = output + "\nIf you forgot your login credentials click the 'forgot login' link.\n\n";
		alert(output);
		return false;
	}
	
	return true;

}



//form validation
function ForgotValidation(theForm) {
	
	var output = "";
	var highlightColor ="#ffe9e6";
	
	// reset form elements to white
	for (i=0; i<theForm.elements.length; i++)
		if (theForm.elements[i].type=="text" || theForm.elements[i].type=="textarea" || theForm.elements[i].type=="select-one")
			theForm.elements[i].style.background='#fff';
		
	// begin validating form elements
	if (theForm.fUserEmail.value == "") {
		output += "- Email\n";
		theForm.fUserEmail.style.background=highlightColor;

	} else if (ValidateEmail(theForm.fUserEmail)==false) { 
		output += "- Not a valid e-mail address!\n";
		theForm.fUserEmail.style.background=highlightColor;
	}

	// Format output (pass or fail validation)
	if (output != "") {
		output = "\nOops! You missed the following required field(s):\n\n" + output;
		//output = output + "\nIf you forgot your login credentials click the 'forgot login' link.\n\n";
		alert(output);
		return false;
	}
	
	return true;

}





function ValidateEmail(field) {
	
	with (field) {
		atpos = value.indexOf("@");
		dotpos = value.lastIndexOf(".");

		if ((atpos < 1) || (dotpos - atpos < 2))
		  return false;
		else 
			return true;
	}
	
}





    
    //form validation
	function UserFormValidation(theForm) {

	var output = "";
	var highlightColor ="#ffe9e6";

	// reset form elements to white
	for (i=0; i<theForm.elements.length; i++)
		if (theForm.elements[i].type=="text" || theForm.elements[i].type=="textarea" || theForm.elements[i].type=="select-one")
			theForm.elements[i].style.background='#fff';
	
	// begin validating form elements
	if (theForm.fFirstname.value == "") {
		output += "- First Name\n";
		theForm.fFirstname.style.background=highlightColor;
	}
	if (theForm.fLastname.value == "") {
		output += "- Last Name\n";
		theForm.fLastname.style.background=highlightColor;
	}
/*	
	if (theForm.fUsername.value == "") {
		output += "- Username\n";
		theForm.fUsername.style.background=highlightColor;
	}
	
	if (theForm.fPassword.value == "") {
		output += "- Password\n";
		theForm.fPassword.style.background=highlightColor;
	}
*/	
	if (theForm.fEmail.value == "") {
		output += "- Email\n";
		theForm.fEmail.style.background=highlightColor;
	}
	else if (theForm.fEmail.value.indexOf("@") < 0) {
		output += "- Email format invalid\n";
		theForm.fEmail.style.background=highlightColor;
	}
	
	RandomLoginGenerator(document.UserForm.fUsername);
	RandomPasswordGenerator(document.UserForm.fPassword);

	// Format output (pass or fail validation)
	if (output != "") {
		output = "\nOops! The following fields are required!\n\n" + output;
		alert(output);
		return false;
	}
	
	return true;
	
}




function RandomLoginGenerator(x) {
	
	var a;
	var b;
	var output;
	
	a = document.UserForm.fLastname.value;
	if (a.length > 6) a = a.substring(0,6);
	
	b = document.UserForm.fFirstname.value;
	b = b.substring(0,1);

	output = a + b
	x.value = output.toLowerCase();
	
	return true;
	
}


function RandomPasswordGenerator(x) {
	
	var theNumber;
	theNumber = Math.floor(Math.random() * 1000000);
	
	x.value = theNumber;
	
	return true;
	
}


function ConfirmActivate(x,y) {

	var agree = confirm("Are you sure you want to activate " + y + "?");
	
	if (!agree)	{
		x.href = "#";
		window.location.reload(false);
	}
				
}




function ConfirmDeactivate(x,y) {

	var agree = confirm("Are you sure you want to deactivate " + y + "?");
	
	if (!agree)	{
		x.href = "#";
		window.location.reload(false);
	}
				
}



function ConfirmReset(x,y) {

	var agree = confirm("Are you sure you want to reset " + y + " category access?\n\nThe user will not be able to access any categories until specific category access has been re-assigned by an administrator.");
	
	if (!agree)	{
		x.href = "#";
		window.location.reload(false);
	}
				
}



function ConfirmActivateAll(x) {

	var agree = confirm("Are you sure you want to activate all?");
	
	if (!agree)	{
		x.href = "#";
		window.location.reload(false);
	}
				
}



function ConfirmDeactivateAll(x) {

	var agree = confirm("Are you sure you want to deactivate all users?");
	
	if (!agree)	{
		x.href = "#";
		window.location.reload(false);
	}
				
}


function ConfirmDelete(x,y) {

	var agree = confirm("Are you sure you want to delete " + y + "?\n\nUser data will be permanently removed from the system.");
	
	if (!agree)	{
		x.href = "#";
		window.location.reload(false);
	}
				
}



function ConfirmRemove(x,y) {

	var agree = confirm("Are you sure you want to remove " + y + " from this category?");
	
	if (!agree)	{
		x.href = "#";
		window.location.reload(false);
	}
				
}


function ConfirmDeleteCategory(x,y) {

	var agree = confirm("If you haven't already archived this category please click 'Cancel' and archive it before you delete it.\n\nClicking 'Ok' will permanently remove all records associated with the category '" + y + "'?");
	
	if (!agree)	{
		x.href = "#";
		window.location.reload(false);
	}
				
}

function ConfirmArchive(x,y,z) {

	var agree = confirm("Are you sure you want to archive the \'" + y + "\' category? \n\nThis category, and all relevant documents, will be moved to a folder called \'" + y + "\' in the archives folder.\n\nIf you wish to change the name of the category click \'Cancel\' and update it\'s name prior to archiving, otherwise click \'OK\' to continue.\n\nNOTE: Archived categories cannot be restored." );
	
	if (!agree)	{
		x.href = "#";
		window.location.reload(false);
	}
				
}





function showhidefield(UserType)
{
	if (UserType.value == 1 ) // Admin User
	{
	  document.getElementById("SelectCategories").style.visibility  = "hidden";
	  document.getElementById("SelectCategories").style.display   = "none";
	  
	  document.getElementById("AdminUser").style.visibility  = "visible";
	  document.getElementById("AdminUser").style.display   = "block";
	  
	  document.getElementById("OpsOUser").style.visibility  = "hidden";
	  document.getElementById("OpsOUser").style.display   = "none";
	}
	
	else if (UserType.value == 2 ) // OpsO User
	{	
	  document.getElementById("SelectCategories").style.visibility  = "hidden";
	  document.getElementById("SelectCategories").style.display   = "none";
	  
	  document.getElementById("AdminUser").style.visibility  = "hidden";
	  document.getElementById("AdminUser").style.display   = "none";
	  
	  document.getElementById("OpsOUser").style.visibility  = "visible";
	  document.getElementById("OpsOUser").style.display   = "block";
	}
	
	else if (UserType.value == 3 ) 
	{
	  document.getElementById("SelectCategories").style.visibility  = "visible";
	  document.getElementById("SelectCategories").style.display   = "block";
	  
	  document.getElementById("AdminUser").style.visibility  = "hidden";
	  document.getElementById("AdminUser").style.display   = "none";
	  
	  document.getElementById("OpsOUser").style.visibility  = "hidden";
	  document.getElementById("OpsOUser").style.display   = "none";
	}
}
